#include "EVSRobotAI.h"
#include "Src/Framework/EVFRobotCore.h"

#include "EVSAlg\EVAlgMecharm.h"
#include "EVSAlg\EVAlgRearDrive.h"
#include "EVSAlg\EVAlgNavigation.h"

#include "EVSAlg\UnitTests\TestServosDirection.h"

#include "Src/Framework/EVFUtils/EVFLogger.h"
#include "Src/Framework/EVFUtils/EVFAssertions.h"

#define RAMP_SLIDE_DURATION_MS		2000		// As proposed by our open-loop experts
#define FWD_VECTOR					FVector(0,1,0)
using namespace EV;
using namespace EVF;
using namespace EVAlg;

RobotAI::RobotAI(RobotCore* core)
	: m_aiObjective(EAIObjective::None)
	, m_aiTarget(nullptr)
	, m_robotCore(core)
{
	if (ev_RobotCore == nullptr)
	{
		//LOG_ERROR("ROBOT CORE NOT INITIALIZED!");
	}

	m_armAlg	= new Mecharm(ev_RobotCore);
	m_navAlg	= new Navigation(ev_RobotCore);
	m_driveAlg	= new RearDrive(this, ev_RobotCore);

	check(m_armAlg && m_navAlg && m_driveAlg);

	// Wait until IMU stabilizes
	delay(3000);

	m_driveAlg->SetTurnRate(0.25);
	m_driveAlg->TurnLeft(90);
}

RobotAI::~RobotAI()
{
}

void RobotAI::Tick(float DeltaTime)
{
	// We need to re-assess if we are lost for 30 seconds!
	if (m_watchdogTimer > 30U * 1000U)
	{
		WhereAmI();
	}

	switch (m_aiObjective)
	{
		case EAIObjective::SearchRamp:
		{
			
		}
		break;

		case EAIObjective::AscendRamp:
		{

		}
		break;

		case EAIObjective::Traverse:
		{
			// Continously monitor distance off the boundary if applicable
			// and issue corrections via the drive.
		}
		break;

		case EAIObjective::DescendRamp:
		{
			OnBeginSlide();
			delay(RAMP_SLIDE_DURATION_MS);
			OnEndSlide();
		}
		break;

		case EAIObjective::SearchPost:
		{

		}
		break;

		default:
		break;

	}

	m_watchdogTimer += DeltaTime;
}

bool RobotAI::IsTickEnabled()
{
	return true;
}

EVAlg::Navigation * EV::RobotAI::GetNavigation() const
{
	return m_navAlg;
}

EVAlg::Mecharm * EV::RobotAI::GetMecharm() const
{
	return m_armAlg;
}

EVAlg::RearDrive * EV::RobotAI::GetDrive() const
{
	return m_driveAlg;
}

void RobotAI::BeginObjective(EAIObjective objective)
{
	switch (objective)
	{
		case EAIObjective::SearchRamp:
		{
			
		}
		break;

		case EAIObjective::AscendRamp:
		{

		}
		break;

		case EAIObjective::Traverse:
		{
		}
		break;

		case EAIObjective::DescendRamp:
		{
		}
		break;

		case EAIObjective::SearchPost:
		{

		}
		break;

		default:
		break;

	}
}

void RobotAI::AlignToRamp( Invokable onFail)
{
	m_aiCache.Clear();
	m_aiCache.Yaw = m_navAlg->GetIMU()->GetRotation().Z;
	
	if (m_aiTarget == nullptr)
		(this->*onFail)();

	if (m_aiTarget->Orientation > m_aiCache.Yaw)
	{
		m_driveAlg->TurnRight(90);
	}
	else
	{
		m_driveAlg->TurnLeft(90);
	}

	m_aiObjective = EAIObjective::AlignRamp;
}

void RobotAI::OnBeginSlide()
{
	m_armAlg->ExtendDown();
}

void RobotAI::OnEndSlide()
{
	m_armAlg->ExtendUp();
}


void RobotAI::OnLocationReached() const
{
}

void RobotAI::OnRotationComplete() const
{
	if (m_aiObjective == EAIObjective::AlignRamp)
	{
		float error = (m_aiCache.Yaw - m_navAlg->GetIMU()->GetRotation().Z());
		if (FMath::Abs(error) > 0.05)
		{
			error > 0 ? m_driveAlg->TurnRight(FMath::ToDegrees(error)) : m_driveAlg->TurnLeft(FMath::ToDegrees(error));
		}
	}
}

void RobotAI::SetupCallbacks()
{
	if (m_driveAlg)
	{
		m_driveAlg->SetOnReachedTargetCallback(&OnLocationReached);
		m_driveAlg->SetOnRotationCompleteCallback(&OnRotationComplete);
	}
}

void RobotAI::WhereAmI()
{
}


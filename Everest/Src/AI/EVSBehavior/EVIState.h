#ifndef EV_ISTATE_H
#define EV_ISTATE_H

namespace EV
{
	class IState;
	class RobotAI;
}

class EV::IState
{
public:
	typedef void(*StateCompleteCallback)();

	virtual ~IState(){}

	void Activate(RobotAI*, StateCompleteCallback);
	virtual void OnExecute() = 0;

protected:
	IState();

	virtual void OnActivate();
	void FinalizeState();

	RobotAI* GetAI() const;
private:

	RobotAI* m_ai;
	StateCompleteCallback m_completeCb;
};
#endif // !EV_ISTATE_H

#include "EVIState.h"
#include "Src/AI/EVSRobotAI.h"

using namespace EV;

void IState::Activate(RobotAI* AI, StateCompleteCallback OnComplete)
{
	m_ai = AI;
	m_completeCb = OnComplete;

	OnActivate();
}

IState::IState()
{
}

void IState::OnActivate()
{
}

void IState::FinalizeState()
{
	m_completeCb();
}

RobotAI* IState::GetAI() const
{
	return m_ai;
}

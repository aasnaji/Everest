#ifndef EV_WALL_ALIGNMENT_H
#define EV_WALL_ALIGNMENT_H

#include "EVIState.h"

namespace EV
{
	class WallAlignment;
}

class EV::WallAlignment : public EV::IState
{
public:
	virtual void OnExecute() override;
protected:
	virtual void OnActivate() override;
private:

};
#endif // !EV_WALL_ALIGNMENT_H


#include "EVSOverrideController.h"

#include "Src/Framework/EVFRobotCore.h"
#include "HardwareSerial.h"

#define KEY_ACCEL_FWD	(EVSKeyMap::W)
#define KEY_TURN_RIGHT  (EVSKeyMap::D)
#define KEY_TURN_LEFT	(EVSKeyMap::A)
#define KEY_ACCEL_BCK   (EVSKeyMap::S)

#define KEY_LARM_UP     (EVSKeyMap::J)
#define KEY_LARM_DOWN   (EVSKeyMap::N)
#define KEY_RARM_UP     (EVSKeyMap::M)
#define KEY_RARM_DOWN   (EVSKeyMap::K)

using namespace EVS;

OverrideController::OverrideController(EVF::RobotCore* core)
	: m_lastInput(EVSKeyMap::None)
	, m_pwmAccel(15)
{
	m_leftWheel = core->GetContinousServo(L_DRIVESERVO_PIN);
	m_rightWheel = core->GetContinousServo(R_DRIVESERVO_PIN);

	Serial.println("Input Controller:");
	Serial.print("My ACCEL FWD KEY is: "); Serial.println(KEY_ACCEL_FWD);
}

OverrideController::~OverrideController()
{
}

void OverrideController::Tick(float DeltaTime)
{
	if (Serial.available() > 0)
	{
		char byte = EVSKeyMap::ToUpper(Serial.read());
		TranslateKeyInput(byte);
	}
}

bool OverrideController::IsTickEnabled()
{
	return true;
}

void OverrideController::TranslateKeyInput(char byte) 
{
	Serial.println(byte);
	KeyEvent type = KeyEvent::Release;
	KeyInput keyInput = KeyInput::None;

	switch (byte)
	{
		case KEY_ACCEL_FWD:
			type = KEY_ACCEL_FWD == m_lastInput ? KeyEvent::Hold : KeyEvent::Press;
			keyInput = KEY_ACCEL_FWD;
			AccelHandler(1);
		break;

		case KEY_ACCEL_BCK:
			AccelHandler(-1);
		break;

		case KEY_TURN_RIGHT:
			type = KEY_TURN_RIGHT == m_lastInput ? KeyEvent::Hold : KeyEvent::Press;
			keyInput = KEY_TURN_RIGHT;
			TurnHandler(-1);
		break;

		case KEY_TURN_LEFT:
			type = KEY_TURN_LEFT == m_lastInput ? KeyEvent::Hold : KeyEvent::Press;
			keyInput = KEY_TURN_LEFT;
			TurnHandler(1);
		break;

		default:
			type = KeyEvent::Release;
			ReleaseHandler();
		break;
	}
}

void OverrideController::LeftArmHandler(int scale, KeyEvent eventType)
{
}

void OverrideController::RightArmHandler(int scale, KeyEvent eventType)
{
}

void OverrideController::AccelHandler(int scale)
{
	Serial.println("Accel Handler");
	if (scale > 0)
	{
		m_leftWheel->SetSpeed(m_leftWheel->GetSpeed() + m_pwmAccel, EVF::Servos::EMoveDirections::Forward);
		m_rightWheel->SetSpeed(m_leftWheel->GetSpeed(), EVF::Servos::EMoveDirections::Backward);
	}
	else
	{
		m_leftWheel->SetSpeed(m_leftWheel->GetSpeed() + m_pwmAccel, EVF::Servos::EMoveDirections::Backward);
		m_rightWheel->SetSpeed(m_leftWheel->GetSpeed(), EVF::Servos::EMoveDirections::Forward);
	}
}

void EVS::OverrideController::TurnHandler(int scale)
{
	Serial.println("Turn Handler");
		m_leftWheel->SetSpeed(m_leftWheel->GetSpeed() + m_pwmAccel*scale);
		m_rightWheel->SetSpeed(m_leftWheel->GetSpeed() - m_pwmAccel*scale);
}

void EVS::OverrideController::ReleaseHandler()
{
	Serial.println("Released Handler");
	/*
	m_leftWheel->SetSpeed(m_leftWheel->GetSpeed() - m_pwmAccel * 2);
	m_rightWheel->SetSpeed(m_rightWheel->GetSpeed() - m_pwmAccel * 2);
	*/
	m_leftWheel->Stop();
	m_rightWheel->Stop();
}

#ifndef EVS_OVERRIDE_CONTROLLER_H
#define EVS_OVERRIDE_CONTROLLER_H

#include "Src/Framework/EVFCore/EVFITickableObject.h"
#include "EVSOverrideKeyMapping.h"

namespace EVS
{
	class OverrideController;

	typedef EVSKeyMap::EKeyboardEventType KeyEvent;
	typedef EVSKeyMap::EKeyInputs KeyInput;
}

namespace EVF
{
	class RobotCore;

	namespace Servos
	{
		class ContinousServo;
	}
}

class EVS::OverrideController : public EVF::ITickableObject
{
public:
	OverrideController(EVF::RobotCore*);
	~OverrideController();

	/* ITickable Interface */
	virtual void Tick(float DeltaTime) override;
	virtual bool IsTickEnabled() override;

	void TranslateKeyInput(char);

	void AccelHandler(int scale);
	void TurnHandler(int scale);
	void LeftArmHandler(int scale, KeyEvent eventType);
	void RightArmHandler(int scale, KeyEvent eventType);


	void ReleaseHandler();

private:
	EVF::Servos::ContinousServo* m_leftWheel;
	EVF::Servos::ContinousServo* m_rightWheel;

	uint8_t m_pwmAccel;

	EVSKeyMap::EKeyInputs m_lastInput;
};
#endif // !EVS_OVERRIDE_CONTROLLER_H


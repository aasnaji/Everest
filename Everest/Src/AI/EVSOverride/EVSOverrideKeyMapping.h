#ifndef EVS_OVERRIDE_KEYMAPPING_H
#define EVS_OVERRIDE_KEYMAPPING_H

namespace EVSKeyMap
{
	enum EKeyboardEventType
	{
		Press = 0x00,
		Hold  = 0x01,
		Release = 0x02
	};

	enum EKeyInputs
	{
		A = 'A',
		B,
		C,
		D,
		E,
		F,
		G,
		H,
		I,
		J,
		K,
		L,
		M,
		N,
		O,
		P,
		Q,
		R,
		S,
		T,
		U,
		V,
		W,
		X,
		Y,
		Z,

		Space = 0x20,
		None = 0x0,
	};

	char ToUpper(char letter);
}
#endif // !EVS_OVERRIDE_KEYMAPPING_H

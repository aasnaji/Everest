#ifndef EV_ALG_REAR_DRIVE_H
#define EV_ALG_REAR_DRIVE_H

#include "Src/Framework/EVFCore/EVFITickableObject.h"
#include "Src/Framework/EVFMath/EVMMath.h"

namespace EV
{
	class RobotAI;
}

namespace EVAlg
{
	class RearDrive;
	class PIDControl;

	typedef void(EV::RobotAI::*ReachedTargetCb)() const;
	typedef void(EV::RobotAI::*RotationCompleteCb)() const;
}

namespace EVF
{
	class RobotCore;
	
	namespace Servos
	{
		class ContinousServo;
	}

	namespace Sensors
	{
		class SensorIMU;
		class SensorCurrent;
	}
}

/*	Rear Drive Algorithm
*
*	The rear drive algorithm manages an internal state of sensory data, caches,
*	and state machines to offer means of producing locomotion using a pair of
*	continous servos in a rear-end assembly. 
*
*	The units of all measurements are in cm unless stated otherwise. The drive 
*	algorithm attempts to produce reliable results by fusing cached distance 
*	values from its sensors and an equation model that predicts movement based
*	on its servo settings.
*
*	As part of the algorithm uses mathematically developed equations, they are 
*	summarized below:
*
*	dtheta/dt =  (Vel_right - Vel_left) / wheelsSpacing
*	theta(t)  =  (Vel_right - Vel_left)*t/ wheelsSpacing  + theta0
*/
class EVAlg::RearDrive : public EVF::ITickableObject
{
public:
	struct FDriveParams
	{
		float TargetTurnAngle ;		// Radians
		uint8_t TargetLinearDistance : 8;
		float	TargetLeftSpeed;
		float	TargetRightSpeed;
	};

	struct FDriveCache
	{
		uint16_t DriveAccumulatedDistance : 16;
		uint16_t IMUAccumulatedDistance : 16;
		float DriveAccumulatedAngle;		// Radians
		float IMUAccumulatedAngle ;		// Radians
	};

	enum class EDriveModes : uint8_t
	{
		Idling,
		Driving,
		Reversing,
		Turning
	};

	RearDrive(const EV::RobotAI* ai, const EVF::RobotCore* core);

	void Hello();

	void MoveForward(float percent);
	void MoveBackward(float percent);

	void MoveDistance(int distance_cm, float atPercent = 1 /* Percent Speed */);

	void TurnLeft(float deltaDegrees);
	void TurnRight(float deltaDegrees);

	void Stop();

	/*	Orient Towards
	*	
	*	Given a position vector in space RELATIVE
	*	to the current position of the robot, orient
	*	the robot to LookAt the target position.
	*/
	void OrientTowards(FVector toVector);

	/*	Step
	*
	*	Attempts to move to a specified position in 3D space
	*	RELATIVE to the current position of the robot
	*/
	void Step(float dx, float dy);

	/*	Accumulated Distance
	*
	*	Returns the total distance traversed by the drive 
	*	algorithm. The distance in cm is a function of the  
	*	wheels' speed, and can be reset to 0.
	*/
	
	/*	Set Acceleration
	*
	*	Sets the acceleration the drive algorithm uses to ramp
	*	to desired speeds. Acceleration is measured in % per second
	*	eventhough it is processed as % per millis due to the Tick time frame
	*/
	void SetAcceleration(float percentPerSecond);

	/*	Set Turn Rate	[DEPRECATED DESCRIPTION]
	*
	*	Sets the turn rate the drive algorithm uses to ramp
	*	to desired angle targets. Turn rate is measured in degrees per second
	*	eventhough it is processed as degrees per millis due to the Tick time frame
	*/
	void SetTurnRate(float percentPerSecond);

	/* ITickableObject Interface */
	virtual void Tick(float DeltaTime) override final;
	virtual bool IsTickEnabled() override final;

	void SetOnReachedTargetCallback(ReachedTargetCb cb);
	void SetOnRotationCompleteCallback(RotationCompleteCb cb);

private:
	/* State Transition Handlers */
	void OnReachedTarget();
	void OnRotationComplete();

	/*	Blend Average
	*
	*	Returns a corrected value between two readings by
	*	averaging them and blending the values via their alpha
	*/
	float BlendAverage(float, float, float);

	void ClearAngleCache();
	void ClearDistanceCache();

	/*	Roughly estimates the expected change in angle (rads) of the body
	*	due to its velocity in the previous time step.
	*/
	float ComputeDeltaAngle(float dt);

	/*	Roughly estimates the expected change in distance (cm) of the body
	*	due to its velocity in the previous time step.
	*/
	float ComputeDeltaDistance(float dt);

	// Callback Handlers
	void OnMaxCurrent(uint8_t pinNumber, float current);
	static void MaxCurrentHandler(uint8_t pinNumber, float current);

private:

	// The continous servos representing the wheels of the Rear Drive Mechanism
	EVF::Servos::ContinousServo* m_LeftWheel;
	EVF::Servos::ContinousServo* m_RightWheel;

	// The continously changing drive params that control the behavior of the drive algorithm
	FDriveParams m_driveParams;
	FDriveCache m_driveCache;

	// The IMU sensor used for feedback control
	const EVF::Sensors::SensorIMU* m_imuSensor;

	EVF::Sensors::SensorCurrent* m_leftWheelSense;
	EVF::Sensors::SensorCurrent* m_rightWheelSense;


	// The PID controller responsible for manipulating the drive
	// parameter of the wheel servos.
	PIDControl* m_PID;

	// To be used when ramping up various speeds
	float m_acceleration;	// % per millis
	float m_turnRate;		// turn speed in %

	EDriveModes m_driveMode;

	// Callbacks
	const EV::RobotAI* m_robotAI;
	ReachedTargetCb m_reachedTargetCb;
	RotationCompleteCb m_rotationCompleteCb;
};
#endif // !EV_ALG_REAR_DRIVE_H

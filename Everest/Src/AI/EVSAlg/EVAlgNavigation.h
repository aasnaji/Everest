#ifndef EV_ALG_NAVIGATION_H
#define EV_ALG_NAVIGATION_H
#include "Src/Framework/EVFCore/EVFITickableObject.h"
#include "Src/Framework/EVFCore/Sensors/EVFSensorIMU.h"
#include "Src/Framework/EVFMath/EVMMath.h"
#include "Src/Framework/EVFSupport/Containers/EVFList.h"

namespace EVAlg
{
	class Navigation;
	class Scanner;
	struct FNavPoint
	{
		unsigned long TimeStamp;
		float RelativeDistance;
		FVector ReportedIMUPosition;
		FVector ReportedIMUOrientation;
	};
	
	enum class FNavPointHint
	{
		Border,
		ObstacleStart,
		ObstacleEnd
	};
}

namespace EVF
{
	class RobotCore;

	namespace Sensors
	{
		class SensorIMU;
		class SensorToF;
		class SensorSonar;
	}
}

class EVAlg::Navigation : public EVF::ITickableObject
{
	enum class ENavLegend
	{
		RampStart = 0,
		RampEnd = 1,
		RampMiddle = 2,
		Origin = 3,
		Target = 4
	};

	/*	Structure to hold geographical data of a location of interest
	*	TimeStamp: The global time in ms the node was visited
	*	AtPosition: Cumulative XYZ position coordinates of the point relative to a starting origin
	*	AtOrientation: Cumulative rotation in rad of the point relative to a starting origin
	*	Legend: Identifies the type of this location of interest (ramp, post, start)
	*/
	struct FVisitNode
	{
		unsigned TimeStamp;
		FVector AtPosition;
		FVector AtOrientation;
		ENavLegend Legend;
	};

public:
	Navigation(const EVF::RobotCore* robotCore);
	~Navigation();

	/* ITickable Interface */
	virtual void Tick(float DeltaTime) override final;
	virtual bool IsTickEnabled() override final;

	const EVF::Sensors::SensorIMU* GetIMU() const;
	const EVAlg::Scanner* GetScanner() const;

	/*	Record Visit
	*
	*	Records the current geographical location and timestamp
	*	to mark the specified NavLegend. The visit can be later 
	*	used to acquire "guesses" for redirection/
	*/
	void RecordVisit(ENavLegend);

	/*	Get Directions To
	*
	*	Returns the rotation in degrees and distance in cm
	*	required to approximately face and reach a desired 
	*	nav legend (if applicable). If the legend was never 
	*	determined or recorded, zero is returned.
	*/
	bool GetDirectionsTo(ENavLegend, uint16_t&, int&);

private:
	void SetupCallbacks();

	/*	On Slope Detect
	*
	*	Notifies the navigation algorithm that a slope has been detected
	*	as part of its traversal. The IMU emitting the event provides the
	*	the most recent data, which in turn is the cause of the event.
	*/
	void OnSlopeDetect(const EVF::Sensors::FSensorReading<EVF::Sensors::FIMUData>& data);

	void OnObstacleDetected(uint16_t distance_mm);
	void OnCollisionEnter(uint16_t distance_mm);
	void OnCollisionExit(uint16_t distance_mm);

	/* Static Callback Helpers */
	static void SlopeDetectHandler(const EVF::Sensors::FSensorReading<EVF::Sensors::FIMUData>& data);
	static void ObstacleDetectHandler(uint16_t distance_mm);
	static void CollisionEnterHandler(uint16_t distance_mm);
	static void CollisionExitHandler(uint16_t distance_mm);
private:
	EVF::Sensors::SensorIMU* m_imu;
	EVAlg::Scanner* m_scanner;

	// Behavioral Components
	FVisitNode m_visitedNodes[5];
};
#endif // !EV_ALG_NAVIGATION_H

#include "EVAlgMecharm.h"
#include "Src/Framework/EVFRobotCore.h"
#include "Src/Framework/EVFCore/Servos/EVFPositionServo.h"

#include "Src/Framework/EVFUtils/EVFAssertions.h"

using namespace EVAlg;
using namespace EVF;

Mecharm::Mecharm(EVF::RobotCore* core)
{
	m_leftArm  = core->GetPositionServo(L_ARMSERVO_PIN);
	m_rightArm = core->GetPositionServo(R_ARMSERVO_PIN);

	if (!m_leftArm->IsValid() || !m_rightArm->IsValid())
	{
		Serial.println("Mecharm init failed!");
		delay(5);
	}
	check(m_leftArm->GetPinNumber() == L_ARMSERVO_PIN);
	check(m_rightArm->GetPinNumber() == R_ARMSERVO_PIN);
}

void Mecharm::Hello()
{
	delay(5);
	for (int i = 0; i < 25; i++)
	{
		m_leftArm->IncrementAngle(20);
		m_rightArm->IncrementAngle(20);

		delay(100);
	}

	delay(3000);
	m_leftArm->SetAngle(0);
	m_rightArm->SetAngle(0);

	for (int i = 0; i < 50; i++)
	{
		m_leftArm->SetAngle(2 * i);
		m_rightArm->SetAngle(2 * i);
	}

	m_leftArm->SetAngle(0);
	m_rightArm->SetAngle(0);
}

void Mecharm::ExtendDown()
{
	m_leftArm->SetAngle(5);
	m_rightArm->SetAngle(5);

	delay(100);
}

void Mecharm::ExtendUp()
{
	m_leftArm->SetAngle(160);
	m_rightArm->SetAngle(160);

	delay(100);
}

��/ /   P I D . c p p   :   T h i s   f i l e   c o n t a i n s   t h e   ' m a i n '   f u n c t i o n .   P r o g r a m   e x e c u t i o n   b e g i n s   a n d   e n d s   t h e r e .  
 / /  
  
 # i n c l u d e   " p c h . h "  
 # i n c l u d e   < i o s t r e a m >  
 # i n c l u d e   < c m a t h >  
 # i n c l u d e   " P I D . h "  
  
  

  
 P I D : : P I D ( )  
 {  
 	 t h i s - > d t   =   0 . 1 ;  
 	 t h i s - > m a x   =   1 0 ;  
 	 t h i s - > m i n   =   - 1 0 ;  
 	 t h i s - > K p   =   1 ;  
 	 t h i s - > K d   =   1 ;  
 	 t h i s - > K i   =   0 . 0 3 ;  
 	 t h i s - > p r e v _ e r r o r   =   0 ;  
 	 t h i s - > i n t e g r a l   =   0 ;  
  
 }  
  

 P I D : : P I D ( f l o a t   d t ,   f l o a t   m a x ,   f l o a t   m i n ,   f l o a t   K p ,   f l o a t   K d ,   f l o a t   K i )  
 {  
 	 t h i s - > d t   =   d t ;   //the sampling time
 	 t h i s - > m a x   =   m a x ;  
 	 t h i s - > m i n   =   m i n ;  
 	 t h i s - > K p   =   K p ;  
 	 t h i s - > K d   =   K d ;  
 	 t h i s - > K i   =   K i ;  
 	 t h i s - > p r e v _ e r r o r   =   0 ;  
 	 t h i s - > i n t e g r a l   =   0 ;  
  
 }  
  
  
  
 f l o a t   P I D : : c a l c u l a t e ( f l o a t   s e t p o i n t ,   f l o a t   o u t p u t )  
 {  
 	 f l o a t   e r r o r   =   s e t p o i n t   -   o u t p u t ;  
 	  
 	 / / C a l c u l a t i n g   I D   o f   e r r o r  
 	 t h i s - > i n t e g r a l   + =   t r a p z ( t h i s - > p r e v _ e r r o r ,   e r r o r ,   t h i s - > d t ) ;  
 	 f l o a t   d e r i v a t i v e   =   d i f f ( t h i s - > p r e v _ e r r o r ,   e r r o r ,   t h i s - > d t ) ;  
  
 	 / / C a l c u l a t i n g   O u t p u t   T e r m s   f o r   P I D  
 	 f l o a t   P o u t   =   t h i s - > K p * e r r o r ;  
 	 f l o a t   I o u t   =   t h i s - > K i * i n t e g r a l ;  
 	 f l o a t   D o u t   =   t h i s - > K d * d e r i v a t i v e ;  
  
 	 / / C a l c u l a t i n g   t o t a l   o u t p u t    
 	 f l o a t   o u t p u t _ t   =   P o u t   +   I o u t   +   D o u t ;  
  
 	 / / T h r e s h o l d i n g   t o   m a x   a n d   m i n   w h e n   t h e   o u t p u t   i s   n o t   w i t h i n   t h e   r a n g e    
 	 i f   ( o u t p u t   >   m a x )  
 	 	 o u t p u t   =   m a x ;  
 	 e l s e   i f   ( o u t p u t   <   m i n )  
 	 	 o u t p u t   =   m i n ;  
  
 	 t h i s - > p r e v _ e r r o r   =   e r r o r ;  
  
 	 r e t u r n   o u t p u t ;  
  
  
 }  
  
 / / C o m p u t e s   i n t e g r a l   o f   d i s c r e t e   p o i n t s   u s i n g   t r a p a z o i d a l   r u l e  
 f l o a t   t r a p z ( f l o a t   f _ a ,   f l o a t   f _ b ,   f l o a t   d t )  
 {  
 	 / / a   i s   s t a r t i n g   t i m e    
 	 / / b   i s   e n d i n g   t i m e    
 	 / / d t   i s   b   -   a  
 	 / / f _ a   i s   v a l u e   a t   t i m e   a    
 	 / / f _ b   i s   v a l u e   a t   t i m e   b  
  
 	 / / b   >   a  
  
 	 r e t u r n   d t * ( ( f _ a   +   f _ b )   /   2 ) ;  
  
 }  
  
 / / C o m p u t e s   d e r i v a t i v e   o f   d i s c r e t e   p o i n t s   u s i n g   f i r s t   f o r w a r d   f i n i t e   d i v i d e d   d i f f e r e n c e    
 f l o a t   d i f f ( f l o a t   f _ a ,   f l o a t   f _ b ,   f l o a t   d t )  
 {  
 	 / / a   i s   s t a r t i n g   t i m e    
 	 / / b   i s   e n d i n g   t i m e    
 	 / / d t   i s   b   -   a  
 	 / / f _ a   i s   v a l u e   a t   t i m e   a    
 	 / / f _ b   i s   v a l u e   a t   t i m e   b  
  
 	 / / b   >   a    
  
 	 r e t u r n   ( f _ b   -   f _ a )   /   d t ;  
 }  
  
  
 / *  
 i n t   m a i n ( )  
 {  
         s t d : : c o u t   < <   " H e l l o   W o r l d ! \ n " ;    
 }  
 * /  
  
 / /   R u n   p r o g r a m :   C t r l   +   F 5   o r   D e b u g   >   S t a r t   W i t h o u t   D e b u g g i n g   m e n u  
 / /   D e b u g   p r o g r a m :   F 5   o r   D e b u g   >   S t a r t   D e b u g g i n g   m e n u  
  
 / /   T i p s   f o r   G e t t i n g   S t a r t e d :    
 / /       1 .   U s e   t h e   S o l u t i o n   E x p l o r e r   w i n d o w   t o   a d d / m a n a g e   f i l e s  
 / /       2 .   U s e   t h e   T e a m   E x p l o r e r   w i n d o w   t o   c o n n e c t   t o   s o u r c e   c o n t r o l  
 / /       3 .   U s e   t h e   O u t p u t   w i n d o w   t o   s e e   b u i l d   o u t p u t   a n d   o t h e r   m e s s a g e s  
 / /       4 .   U s e   t h e   E r r o r   L i s t   w i n d o w   t o   v i e w   e r r o r s  
 / /       5 .   G o   t o   P r o j e c t   >   A d d   N e w   I t e m   t o   c r e a t e   n e w   c o d e   f i l e s ,   o r   P r o j e c t   >   A d d   E x i s t i n g   I t e m   t o   a d d   e x i s t i n g   c o d e   f i l e s   t o   t h e   p r o j e c t  
 / /       6 .   I n   t h e   f u t u r e ,   t o   o p e n   t h i s   p r o j e c t   a g a i n ,   g o   t o   F i l e   >   O p e n   >   P r o j e c t   a n d   s e l e c t   t h e   . s l n   f i l e  
  
  
 

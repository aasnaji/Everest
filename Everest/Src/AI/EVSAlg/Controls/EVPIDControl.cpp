#include "EVPIDControl.h"
#include "Src/Framework/EVFMath/EVMMath.h"

using namespace EVAlg;

PIDControl::PIDControl(const FPIDParams& params)
	: m_parameters(params)
{
}

PIDControl::PIDControl(float Kp, float Kd, float Ki, float MinRange, float MaxRange)
	: m_parameters(FPIDParams(Kp,Kd,Ki,MinRange,MaxRange))
{
}

void PIDControl::SetAppliedInput(float setPoint)
{
	m_setPoint = setPoint;
}

void PIDControl::ProcessLoop(float& prevError, float& currError, float dt)
{
	float P = m_parameters.Kp * currError; 
	float I = m_parameters.Ki * FMath::Cumtrapz(prevError, currError, dt);		
	float D = m_parameters.Kd * FMath::Differentiate(prevError,currError,dt);	

	prevError = currError;
	currError = currError - FMath::Clamp(m_parameters.MinRange, m_parameters.MaxRange, P + I + D); // Tip: Need to express change in plant parameter as function of currError
}

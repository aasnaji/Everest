#ifndef EV_PID_CONTROL_H
#define EV_PID_CONTROL_H
#include "EVIBaseControls.h"

namespace EVAlg
{
	class PIDControl;

	struct FPIDParams
	{
	public:
		FPIDParams(float _Kp, float _Kd, float _Ki, float _MinRange, float _MaxRange)
			: Kp(_Kp)
			, Kd(_Kd)
			, Ki(_Ki)
			, MinRange(_MinRange)
			, MaxRange(_MaxRange)
		{
		}

		float Kp, Kd, Ki;
		float MinRange, MaxRange;
	};
}

class EVAlg::PIDControl : public EVAlg::IBaseControls
{
public:
	PIDControl(const FPIDParams& params);
	PIDControl(float Kp, float Kd, float Ki, float MinRange, float MaxRange);
	//float Calculate(float setpoint, float prev_value);
	
	/* IBaseControls Interface */
	virtual void SetAppliedInput(float setPoint) override;
	virtual void ProcessLoop(float& prevError, float& currError, float dt) override;

private:
	float m_setPoint;

	float dt;
	float prev_error;
	float integral;

	const FPIDParams m_parameters;
};
#endif // !EV_PID_CONTROL_H

#ifndef EVF_IBASE_CONTROLS_H
#define EVF_IBASE_CONTROLS_H

namespace EVAlg
{
	class IBaseControls;
}

/*	IBaseControls
*
*	An interface that provides an input/output
*	behaviour to an automatic controller.
*
*	All automatic control systems operate by being fed an input value,
*	the system internally implements its custom controls logic and processes
*	the input to provide a corresponding output value.
*/
class EVAlg::IBaseControls
{
public:
	virtual ~IBaseControls() {};

	/*	A specialized controller intakes an input value
	*	and provides a corresponding value via the implementation's
	*	transfer function. Ex: P, PD, PID Controllers
	*/
	virtual void SetAppliedInput(float input) = 0;

	/*	Process loops evaluates the closed loop response of the system
	*	for given values of error of the current and previous time step.
	*	The error values are modified to reflect the changes due to the
	*	output of the system
	*/
	virtual void ProcessLoop(float& prevError, float& currError, float dt) = 0;
	
protected:
	IBaseControls() {} // Disables direct instantiation of the interface
};
#endif // !EVF_IBASE_CONTROLS_H

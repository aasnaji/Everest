#pragma once
#ifndef _PID_H_
#define _PID_H_

class PID
{
	public: 
		PID();
		PID(float dt, float max, float min, float Kp, float Kd, float Ki);
		float calculate(float setpoint, float prev_value);
		~PID();
	private:
		float dt;
		float max;
		float min;
		float Kp;
		float Kd;
		float Ki; 
		float prev_error;
		float integral;

};


//Computes integral of discrete points using trapazoidal rule
float trapz(float f_a, float f_b, float dt);

//Computes derivative of discrete points using first forward finite divided difference 
float diff(float f_a, float f_b, float dt);


#endif 

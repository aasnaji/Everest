#include "EVAlgNavigation.h"
#include "EVAlgScanner.h" 

#include "Src/Framework/EVFRobotCore.h"
#include "Src/Framework/EVFCore/Sensors/EVFSensorToF.h"
#include "Src/Framework/EVFCore/Sensors/EVFSensorIMU.h"

#include "Src/Framework/EVFUtils/EVFAssertions.h"
#include "Src/Framework/EVFUtils/EVFLogger.h"

#define SLOPE_XACCEL_THRESHOLD 9.58 // The actual value of the X-component of acceleration. A value less than this indicates a slope change
#define SLOPE_ZACCEL_THRESHOLD 0.35

#define FWD_VECTOR				(FVector(0,1,0))


using namespace EVAlg;
using namespace EVF;

static Navigation* instance = nullptr;

Navigation::Navigation(const EVF::RobotCore* robotCore)
{
	m_imu	= (Sensors::SensorIMU*)robotCore->GetSensorOfType(Sensors::ESensorType::IMU, PIN_UNDEFINED);
	m_scanner = new Scanner(robotCore);

	check(m_imu && m_tof && m_scanner);

	instance = this;

	// Reduce the damping of the filter to better detect slopes
	m_imu->SetFilterSamples(0);

	SetupCallbacks();
}

Navigation::~Navigation()
{
}


void Navigation::Tick(float DeltaTime)
{
}

bool Navigation::IsTickEnabled()
{
	return true;
}

const EVF::Sensors::SensorIMU* Navigation::GetIMU() const
{
	return m_imu;
}

const EVAlg::Scanner* Navigation::GetScanner() const
{
	return m_scanner;
}

void Navigation::RecordVisit(ENavLegend legend)
{
	uint8_t id = static_cast<uint8_t>(legend);
	
	m_visitedNodes[id].AtPosition = m_imu->GetPosition();
	m_visitedNodes[id].AtOrientation = m_imu->GetRotation();
	m_visitedNodes[id].TimeStamp = millis();
	m_visitedNodes[id].Legend = legend;
}

bool Navigation::GetDirectionsTo(ENavLegend legend, uint16_t& outDistance_cm, int& outAngleDeg)
{
	FVisitNode node = m_visitedNodes[static_cast<uint8_t>(legend)];

	// Never Recorded
	if(node.TimeStamp == 0)
		return false;

	// Recall require a conversion from meters to cm
	FVector dir = node.AtPosition - m_imu->GetPosition() * 100.0;

	// Find the angle between our current forward direction, and the direction vector
	outAngleDeg = FMath::ToDegrees((node.AtOrientation - m_imu->GetRotation()).X());

	// Need to revisit this; this assumes X-Y plane of motion, however the IMU is mounted such that
	// the plane of motion is on the Y-Z plane!
	outDistance_cm = dir.Length2D(); 

	return true;
}

void Navigation::SetupCallbacks()
{
	if (m_imu)
	{
		m_imu->SetSlopeDetectCallback(SlopeDetectHandler);
	}

	if (m_scanner)
	{
		m_scanner->GetToF()->SetObstacleCallback(ObstacleDetectHandler);
		m_scanner->GetToF()->SetCollisionEnterCallback(CollisionEnterHandler);
		m_scanner->GetToF()->SetCollisionExitCallback(CollisionExitHandler);
	}
}

void Navigation::OnSlopeDetect(const EVF::Sensors::FSensorReading<EVF::Sensors::FIMUData>& data)
{
	// Use Time stamps to eliminate faulty signals
	// Indicates downward slope
	if (data.Reading.Accel.X() < SLOPE_XACCEL_THRESHOLD && data.Reading.Accel.Z() >= SLOPE_ZACCEL_THRESHOLD)
	{
		FVisitNode node;

		node.TimeStamp = data.TimeStamp;
		node.AtPosition = m_imu->GetPosition();
		node.AtOrientation = m_imu->GetRotation();
		node.Legend = ENavLegend::RampStart;

		m_visitedNodes[static_cast<uint8_t>(node.Legend)] = node;

		Serial.println("Ramp Up!");
		delay(5);
	}
	
	// Indicates upward spiral
	else if (data.Reading.Accel.X() < SLOPE_XACCEL_THRESHOLD && data.Reading.Accel.Z() <= -SLOPE_ZACCEL_THRESHOLD)
	{
		FVisitNode node;

		node.TimeStamp = data.TimeStamp;
		node.AtPosition = m_imu->GetPosition();
		node.AtOrientation = m_imu->GetRotation();
		node.Legend = ENavLegend::RampEnd;

		m_visitedNodes[static_cast<uint8_t>(node.Legend)] = node;

		Serial.println("Ramp Down");
		delay(5);
	}

	// Indicates a transition to a flat surface
	else if (data.Reading.Accel.X() > SLOPE_XACCEL_THRESHOLD)
	{
		FVisitNode node;

		node.TimeStamp = data.TimeStamp;
		node.AtPosition = m_imu->GetPosition();
		node.AtOrientation = m_imu->GetRotation();
		node.Legend = ENavLegend::RampMiddle;

		m_visitedNodes[static_cast<uint8_t>(node.Legend)] = node;

		Serial.println("Ramp Flat");
		delay(5);
	}
}

void Navigation::OnObstacleDetected(uint16_t distance_mm)
{
	Serial.println("Obstacle Detected");
	delay(1);
}

void Navigation::OnCollisionEnter(uint16_t distance_mm)
{
	Serial.println("Collision Entered");
	delay(1);
}

void Navigation::OnCollisionExit(uint16_t distance_mm)
{
	Serial.println("CollisionExit");
	delay(1);
}




void Navigation::SlopeDetectHandler(const EVF::Sensors::FSensorReading<EVF::Sensors::FIMUData>& data)
{
	if (instance)
	{
		instance->OnSlopeDetect(data);
	}
}

void Navigation::ObstacleDetectHandler(uint16_t distance_mm)
{
	if (instance)
		instance->OnObstacleDetected(distance_mm);
}

void Navigation::CollisionEnterHandler(uint16_t distance_mm)
{
	if (instance)
		instance->OnCollisionEnter(distance_mm);
}

void Navigation::CollisionExitHandler(uint16_t distance_mm)
{
	if (instance)
		instance->OnCollisionExit(distance_mm);
}

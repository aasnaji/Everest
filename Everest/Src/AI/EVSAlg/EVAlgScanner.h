#ifndef EV_ALG_SCANNER_H
#define EV_ALG_SCANNER_H

#include "Src/Framework/EVFSupport/Containers/EVFList.h"

#include "stdint.h"
namespace EVAlg
{
	class Scanner;

	enum class EScanHint : uint8_t
	{
		Border,
		Obstacle,
		Unknown
	};

	/*	Structure that holds information of scan hit result
	*	The scanner reports the time of measurement, distances
	*	from both Sonar and ToF, and the angle the radar was facing,
	*	alongside a hint as to whether it hit an obstacle or border
	*/
	struct FScanHit
	{
		unsigned long TimeStamp;
		float SonarDistance;		// Can use uint16 instead if we use mm measurements
		float ToFDistance;
		uint8_t  Angle;
		EScanHint HitInfo;
	};
}

namespace EVF
{
	class RobotCore;
	namespace Sensors
	{
		class SensorSonar;
		class SensorToF;
	}

	namespace Servos
	{
		class PositionServo;
	}
}

class EVAlg::Scanner
{
public:
	// Scan Resolution governs the number of degrees of rotation per scan
	enum class EScanResolution : uint8_t
	{
		Low = 10,	
		Medium = 5,
		High = 1
	};

	Scanner( const EVF::RobotCore* core);
	~Scanner();

	/*	Scan Area
	*
	*	Performs a scan of the area by sweeping the
	*	sonar across a range of angles from 0-180.
	*
	*	The sweep scan returns a list of points of 
	*	interest relative to the location of the current
	*	scan.
	*/
	EVF::TList<FScanHit> SweepScan(int fromAngle, int toAngle);
	FScanHit Scan();

	/*	Set Scan Resolution
	*
	*	Set a governing scan resolution that determines the
	*	number of incremental rotations the scanners sweeps by.
	*
	*	Low:	10 degrees per rotation
	*	Medium: 5 degrees per rotation
	*	High:	1 degree per rotation
	*/
	void SetScanResolution(EScanResolution res);

	EVF::Sensors::SensorToF* GetToF() const;

private:
	/* Post process scan results to determine hints about the hit */
	void ProcessScanHints(EVF::TList<FScanHit>& scans);

private:
	EVF::Servos::PositionServo* m_servo;
	EVF::Sensors::SensorSonar* m_sonar;
	EVF::Sensors::SensorToF* m_tof;

	EScanResolution m_resolution;
};
#endif // !EV_ALG_SCANNER_H



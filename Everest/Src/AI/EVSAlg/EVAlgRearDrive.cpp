#include "EVAlgRearDrive.h"
#include "Controls\PID.h"
#include "Src/AI/EVSRobotAI.h"

#include "Src/Framework/EVFRobotCore.h"
#include "Src/Framework/EVFCore/Servos/EVFContinousServo.h"
#include "Src/Framework/EVFCore/Sensors/EVFSensorIMU.h"
#include "Src/Framework/EVFCore/Sensors/EVFSensorCurrent.h"

#include "Src/Framework/EVFUtils/EVFAssertions.h"

using namespace EVAlg;
using namespace EVF;

#define WHEEL_RADIUS_CM							(2.5)		// Wheel radius in meters
#define WHEEL_SHAFT_LENGTH_CM					(5.0)		// Wheel to wheel distance - drive shaft length in meters
#define DISTANCE_CLEARANCE_CM					(1.0)		// Distance threshold in cm to be considered in clearance
#define ANGLE_CLEARANCE_RAD						(0.05)

#define DEBUG_DRIVE

static RearDrive* instance = nullptr;

static const FVector& Forward = FVector(0, 1, 0);
static FVector PrevIMUPos = FVector();
static float PrevGyroAngle = 0;

EVAlg::RearDrive::RearDrive(const EV::RobotAI* ai, const EVF::RobotCore* core)
	: m_robotAI(ai)
	, m_acceleration(0.001 /* % */) // As they always say, go from 0 to 60 in 1 second...
	, m_turnRate(0.5 /* % */)
	, m_driveMode(EDriveModes::Idling)
{
	// Only one instance is allowed!
	check(!instance);

	// Initially at rest, no desired motion
	m_driveParams.TargetLinearDistance = 0;
	m_driveParams.TargetTurnAngle = 0;
	m_driveParams.TargetRightSpeed = 0;
	m_driveParams.TargetLeftSpeed = 0;

	m_LeftWheel = core->GetContinousServo(L_DRIVESERVO_PIN);
	m_RightWheel = core->GetContinousServo(R_DRIVESERVO_PIN);

	check(m_LeftWheel && m_RightWheel);

	m_imuSensor = (const Sensors::SensorIMU*)(core->GetSensorOfType(Sensors::ESensorType::IMU, PIN_UNDEFINED));
	check(m_imuSensor);

	m_leftWheelSense = (Sensors::SensorCurrent*)(core->GetSensorOfType(Sensors::ESensorType::CurrentSense, L_DRIVESERVO_SENSE_PIN));
	m_rightWheelSense = (Sensors::SensorCurrent*)(core->GetSensorOfType(Sensors::ESensorType::CurrentSense, R_DRIVESERVO_SENSE_PIN));
	
	// Override Servo Profiles as per application required
	m_LeftWheel->ApplyServoProfile<Servos::ContServoLProfile>();
	m_RightWheel->ApplyServoProfile<Servos::ContServoRProfile>();

	// Setup Callbacks
	m_leftWheelSense->SetMaxCurrentCallback(MaxCurrentHandler);
	m_rightWheelSense->SetMaxCurrentCallback(MaxCurrentHandler);
}

void EVAlg::RearDrive::Hello()
{
	delay(1);
	m_LeftWheel->SetSpeed(1);
	m_RightWheel->SetSpeed(1, Servos::EMoveDirections::Backward);
	delay(3000);

	m_LeftWheel->SetSpeed(1, Servos::EMoveDirections::Backward);
	m_RightWheel->SetSpeed(1);
	delay(3000);

	m_LeftWheel->Stop();
	m_RightWheel->Stop();
}


void EVAlg::RearDrive::Step(float dx, float dy)
{
}


void RearDrive::SetAcceleration(float percentPerSecond)
{
	m_acceleration = FMath::Clamp(0, 1, percentPerSecond/1000.0);	// Conversion to % / millis
}

void RearDrive::SetTurnRate(float percentPerSecond)
{
	// Find the required Vel to achieve dtheta/dt * (180/M_PI) knowing that:
	// * t = 1
	// * Vel_right = - Vel_left
	// * wheelSpacing = WHEEL_SHAFT_LENGTH_CM
	//dtheta / dt = (Vel_right - Vel_left) / wheelsSpacing
	m_turnRate = FMath::Clamp(0, 1, percentPerSecond / 1000.0);		// Conversion to % / millis
}

void EVAlg::RearDrive::Tick(float DeltaTime)
{
	switch (m_driveMode)
	{
		/*	IDLE CASE
		*	
		*	Simply ensure no wheel movement is happening
		*/
		case EDriveModes::Idling:
		{
			if (m_LeftWheel->GetSpeed() > 0 || m_RightWheel->GetSpeed() > 0)
			{
				m_LeftWheel->Stop();
				m_RightWheel->Stop();
			}
		}
		break;

		/*	DRIVING CASE
		*
		*	Attempt to continously drive to a target distance. If no distance 
		*	is specified ( = 0), keep driving indefinately until notified otherwise
		*/
		case EDriveModes::Driving:
		case EDriveModes::Reversing:
		{
			Servos::EMoveDirections dir = m_driveMode == EDriveModes::Driving 
										? Servos::EMoveDirections::Forward 
										: Servos::EMoveDirections::Backward;

			// Accelerates both values using the set acceleration percentage while ensuring the target speed is not surpassed.
			m_LeftWheel->SetSpeed(FMath::Clamp(0, m_driveParams.TargetLeftSpeed, m_LeftWheel->GetSpeed() + m_acceleration * DeltaTime), dir);
			m_RightWheel->SetSpeed(FMath::Clamp(0, m_driveParams.TargetRightSpeed, m_LeftWheel->GetSpeed() + m_acceleration * DeltaTime), dir);

			m_driveCache.IMUAccumulatedDistance += (m_imuSensor->GetPosition() - PrevIMUPos).Length2D();
			m_driveCache.DriveAccumulatedDistance += ComputeDeltaDistance(DeltaTime); // Equation Model here

			// The IMU will exhibit drag when acquiring position, assign as much weight for the model
			float correctedDistance = BlendAverage(	  m_driveCache.IMUAccumulatedDistance
													, m_driveCache.DriveAccumulatedDistance
													, 0.0f);

			// Update the previous position vector
			PrevIMUPos = m_imuSensor->GetPosition();

			#ifdef DEBUG_DRIVE
			Serial.print("IMU Distance: "); Serial.println(m_driveCache.IMUAccumulatedDistance);
			Serial.print("Corrected Distance:"); Serial.println(correctedDistance);
			#endif // DEBUG_DRIVE

			// Indicates no target distance is set
			if (m_driveParams.TargetLinearDistance == 0)
				return;

			// Compare and assess whether we achieved our targetted traversal
			if (FMath::Abs(correctedDistance - m_driveParams.TargetLinearDistance) <= DISTANCE_CLEARANCE_CM)
			{
				OnReachedTarget();
			}
		}
		break;

		case EDriveModes::Turning:
		{
			m_driveCache.IMUAccumulatedAngle += m_imuSensor->GetCurrGyro().Z() * DeltaTime / 1000.0;		// Need conversion because gyro reads rads/sec
			m_driveCache.DriveAccumulatedAngle = (m_imuSensor->GetRotation().Z() - PrevGyroAngle);//ComputeDeltaAngle(DeltaTime);								// Equation Model here

#ifdef DEBUG_DRIVE
			Serial.print("Target Angle: "); Serial.println(m_driveParams.TargetTurnAngle);
			Serial.print("Accumulated Angle (IMU): "); Serial.println(m_driveCache.IMUAccumulatedAngle);
			Serial.print("Accumulated Angle (MDL): "); Serial.println(m_driveCache.DriveAccumulatedAngle);
#endif // DEBUG_DRIVE

			// We trust the IMU more because it is a direct reading , assign it more weight
			float correctedAngle = BlendAverage(  m_driveCache.IMUAccumulatedAngle
												, m_driveCache.DriveAccumulatedAngle
												, 0.0f);

#ifdef DEBUG_DRIVE
			Serial.print("Corrected Angle: "); Serial.println(correctedAngle);
#endif // DEBUG_DRIVE

			if (FMath::Abs(correctedAngle - m_driveParams.TargetTurnAngle) <= ANGLE_CLEARANCE_RAD)
			{
				OnRotationComplete();
			}
		}
		break;
	}

	// Update the position cache
	PrevIMUPos = m_imuSensor->GetPosition();
}

bool EVAlg::RearDrive::IsTickEnabled()
{
	return true;
}

void RearDrive::OnReachedTarget()
{
	m_driveMode = EDriveModes::Idling;

	// Notify Listeners
	if (m_robotAI)
		(m_robotAI->*m_reachedTargetCb)();

	ClearDistanceCache();
}

void RearDrive::OnRotationComplete()
{
	Serial.println("Rotation Complete");
	m_driveMode = EDriveModes::Idling;

	// Notify Listeners
	if (m_robotAI)
		(m_robotAI->*m_rotationCompleteCb)();

	ClearAngleCache();
}

void RearDrive::SetOnReachedTargetCallback(ReachedTargetCb cb)
{
	m_reachedTargetCb = cb;
}

void RearDrive::SetOnRotationCompleteCallback(RotationCompleteCb cb)
{
	m_rotationCompleteCb = cb;
}


void RearDrive::MoveForward(float percent)
{
	// Clear previously accumulated distances
	ClearDistanceCache();

	m_driveMode = EDriveModes::Driving;

	m_driveParams.TargetLeftSpeed = percent;
	m_driveParams.TargetRightSpeed = percent;
	m_driveParams.TargetLinearDistance = 0; // Indefinite movement
}

void RearDrive::MoveBackward(float percent)
{
	ClearDistanceCache();
	m_driveMode = EDriveModes::Reversing;

	m_driveParams.TargetLeftSpeed = percent;
	m_driveParams.TargetRightSpeed = percent;
	m_driveParams.TargetLinearDistance = 0; // Indefinite movement
}

void RearDrive::MoveDistance(int distance_cm, float atPercent /* Percent Speed */)
{
	ClearDistanceCache();

	m_driveMode = distance_cm > 0 ? EDriveModes::Driving : EDriveModes::Reversing;

	/* Restrict to a maximum linear distance of 255 cm*/
	m_driveParams.TargetLeftSpeed = atPercent;
	m_driveParams.TargetRightSpeed = atPercent;
	m_driveParams.TargetLinearDistance = FMath::Clamp(0, 255, FMath::Abs(distance_cm));
}

void RearDrive::TurnLeft(float deltaDegrees)
{
	ClearAngleCache();
	m_driveMode = EDriveModes::Turning;
	
	m_driveParams.TargetTurnAngle = FMath::ToRadians(deltaDegrees);
	PrevGyroAngle = m_imuSensor->GetRotation().Z();

	// We execute turns at a constant speed, value does not update in tick
	m_LeftWheel->SetSpeed(m_turnRate, Servos::EMoveDirections::Backward);
	m_RightWheel->SetSpeed(m_turnRate, Servos::EMoveDirections::Forward);
}

void RearDrive::TurnRight(float deltaDegrees)
{
	ClearAngleCache();
	m_driveMode = EDriveModes::Turning;

	m_driveParams.TargetTurnAngle = -FMath::ToRadians(deltaDegrees);
	PrevGyroAngle = m_imuSensor->GetRotation().Z();

	// We execute turns at a constant speed, value does not update in tick
	m_LeftWheel->SetSpeed(m_turnRate, Servos::EMoveDirections::Forward);
	m_RightWheel->SetSpeed(m_turnRate, Servos::EMoveDirections::Backward);
}

void RearDrive::Stop()
{
	m_driveMode = EDriveModes::Idling;
	ClearDistanceCache();
	ClearAngleCache();
}

void RearDrive::OrientTowards(FVector toVector)
{
	float requiredRotation = FMath::ToDegrees(FVector::AngleBetween(Forward, toVector));
	
	// Update the drive's state machine to turn to the desired orientation
	requiredRotation > 0 ? TurnRight(requiredRotation) : TurnLeft(requiredRotation);
}

// Primary value takes precedence. When the alpha is zero, the resulting calculation
// is the value PrimaryVal. If set to 1, the resulting calculation is the average of
// the Primary and Secondary Values.
float RearDrive::BlendAverage(float PrimaryVal, float SecondaryVal, float alpha)
{
	return FMath::Lerp(PrimaryVal, (PrimaryVal + SecondaryVal) / 2.0, alpha);
}

void RearDrive::ClearAngleCache()
{
	m_driveCache.DriveAccumulatedAngle = 0;
	m_driveCache.IMUAccumulatedAngle = 0;
}

void RearDrive::ClearDistanceCache()
{
	PrevIMUPos = m_imuSensor->GetPosition();
	m_driveCache.DriveAccumulatedDistance = 0;
	m_driveCache.IMUAccumulatedDistance = 0;
}

// In radians
float RearDrive::ComputeDeltaAngle(float dt_ms)
{
	float v1 = m_LeftWheel->GetSpeedRad() * WHEEL_RADIUS_CM / 100.0;
	float v2 = m_RightWheel->GetSpeedRad()* WHEEL_RADIUS_CM / 100.0;

	return (dt_ms/1000.0) * (v1+v2) / WHEEL_SHAFT_LENGTH_CM / 100.0;
}

// In centimeters
float RearDrive::ComputeDeltaDistance(float dt_ms)
{
	float d1 = m_LeftWheel->GetSpeedRad() * WHEEL_RADIUS_CM * (dt_ms / 1000.0);
	float d2 = m_RightWheel->GetSpeedRad() * WHEEL_RADIUS_CM * (dt_ms /1000.0);
	
	return d1 + d2;
}

void RearDrive::OnMaxCurrent(uint8_t pinNumber, float current)
{
	if (m_leftWheelSense->GetPinNumber() == pinNumber)
	{
		// Respond...
	}
	else if (m_rightWheelSense->GetPinNumber() == pinNumber)
	{
		// Respond...
	}
}

/* Callback Handlers */
void RearDrive::MaxCurrentHandler(uint8_t pinNumber, float current)
{
	if (instance)
	{
		instance->OnMaxCurrent(pinNumber, current);
	}
}


#include "EVAlgScanner.h"
#include "Src/Framework/EVFRobotCore.h"

#include "Src/Framework/EVFCore/Sensors/EVFSensorSonar.h"
#include "Src/Framework/EVFCore/Servos/EVFPositionServo.h"

#include "Src/Framework/EVFCore/Sensors/EVFSensorSonar.h"
#include "Src/Framework/EVFCore/Sensors/EVFSensorToF.h"

#include "Src/Framework/EVFUtils/EVFAssertions.h"
#include "Src/Framework/EVFUtils/EVFLogger.h"
#include "Src/Framework/EVFMath/EVMMath.h"

#define TOF_SONAR_OVERLAP_THRESHOLD			3 //cm
#define	TOF_MAX_RANGE						50
#define SONAR_MAX_RANGE						200
#define	OBSTALCE_EDGE_THRESHOLD				5

using namespace EVAlg;
using namespace EVF;

Scanner::Scanner(const RobotCore* core)
	:m_resolution(EScanResolution::Medium)
{
	m_servo = core->GetPositionServo(SONAR_SERVO_PIN);
	m_sonar = (Sensors::SensorSonar*)(core->GetSensorOfType(Sensors::ESensorType::Sonar, SONAR_TRIGGER_PIN));
	m_tof	= (Sensors::SensorToF*)(core->GetSensorOfType(Sensors::ESensorType::ToF, PIN_UNDEFINED));

	m_tof->SetToFMode(Sensors::SensorToF::EToFModes::LongRange);
}

EVAlg::Scanner::~Scanner()
{
}

TList<FScanHit> Scanner::SweepScan(int fromAngle, int toAngle)
{
	m_servo->SetAngle(fromAngle);
	//m_remainingScans = FMath::Abs(toAngle - fromAngle) / static_cast<uint8_t>(m_resolution);
	
	// Find out how many iterations we need based on our scan resolution
	int steps = FMath::Abs((toAngle - fromAngle) / static_cast<uint8_t>(m_resolution));
	int dir = toAngle > fromAngle ? 1 : -1;

	// Iterate and increment by our scan resolution
	TList<FScanHit> scanHits;
	for (int i = 0; i < steps; i++)
	{
		m_servo->IncrementAngle(dir * static_cast<uint8_t>(m_resolution));

		// Scan
		FScanHit hit = Scan();

		// The orientation of the servo when the hit occured is our initial position
		// offset by our incremental steps.
		hit.Angle = fromAngle + i*steps*dir;
		scanHits.Insert(hit);
	}

	// Re-interprets the scan hits based on the most recent addition
	ProcessScanHints(scanHits);
	return scanHits;
}

FScanHit Scanner::Scan()
{
	float distance_cm_sonar = m_sonar->RequestMeasurement();
	float distance_cm_tof = m_tof->GetCurrDistance() / 10;
	FScanHit hit;

	hit.SonarDistance = distance_cm_sonar;
	hit.ToFDistance = distance_cm_tof;
	hit.TimeStamp = millis();

	return hit;
}

void Scanner::SetScanResolution(EScanResolution res)
{
	m_resolution = res;
}

Sensors::SensorToF* Scanner::GetToF() const
{
	return m_tof;
}

/*	Scan Hint Rules:
*
*	The Sonar is mounted is higher than the ToF. As such, the 
*	Sonar should always record an out of range reading unless it encounters
*	a tall obstacle such (post, wall). In that case, both the ToF and Sonar will
*	have comparable values. On the other hand, the ToF measures distances of borders
*	and unknown obstacles within the course.
*
*	if ToF - Sonar < Overlap Threshold -> Detected Post/Wall/Obstacle
*	if ToF - Sonar > Overlap Threshold -> Border or Ramp
*
*/
void Scanner::ProcessScanHints(TList<FScanHit>& scans)
{
	auto it = scans.Begin();

	for (; it != scans.End(); it++)
	{
		// Out of range readings, we cannot interpret this data
		if ((*it).SonarDistance >= SONAR_MAX_RANGE && (*it).ToFDistance >= TOF_MAX_RANGE)
		{
			(*it).HitInfo = EScanHint::Unknown;
		}

		// The ToF and Sonar have overlapping values
		if (FMath::Abs((*it).ToFDistance - (*it).SonarDistance) <= TOF_SONAR_OVERLAP_THRESHOLD)
		{
			(*it).HitInfo = EScanHint::Obstacle;
		}

		// Ignore the Sonar - only the ToF measures on-course distance
		else
		{
			(*it).HitInfo = (*it).ToFDistance > TOF_MAX_RANGE ? EScanHint::Unknown : EScanHint::Border;
		}
	}
}






/*	DEPRECATED CODE SEGMENT
*	
*	DISGUSTING CONDITIONAL FOR SCAN HINT PROCESSING (GOOD RIDDANCE!)

// Process ToF distance alone first

// If the edge threshold is reached, the two readings represent separate objects
if ((*it).ToFDistance - prevTofDist > OBSTALCE_EDGE_THRESHOLD)
{
// The current object is farther due its larger distance reading. If the
// previous reading was a border, then the farther reading HAS to be a border as well
if (prevHint == EScanHint::Border || (*it).ToFDistance > TOF_MAX_RANGE)
{
(*it).HitInfo = EScanHint::Border;
}
// We broke off an obstacle - this indicates a transition from an obstacle to the border
else if (prevHint == EScanHint::ObstacleFace)
{
auto it_prev = it;
it_prev--;

// Mark the end of the previous obstacle
(*it_prev).HitInfo = EScanHint::ObstacleEnd;
(*it).HitInfo = EScanHint::Border;
}

// Strange reading, can occur if the previous reading hit a sharp edge
// this reading could be a border edge, or the edge of an obstacle
// assume it is an obstacle edge, and defer judgment to navigational algorithm
// the current reading is assumed to be a border.
else if (prevHint == EScanHint::ObstacleStart)
{
(*it).HitInfo = EScanHint::Border;
}

// The more obvious case; if we somehow know the previous point is an obstacle end,
// we expect this farther reading to be a border
else if (prevHint == EScanHint::ObstacleEnd)
{
(*it).HitInfo = EScanHint::Border;
}

// Simply assume border when in doubt...
else
{
(*it).HitInfo == EScanHint::Border;
}
}

// Indicates the current reading is CLOSER to us than the previous
else if ((*it).ToFDistance - prevTofDist < -OBSTALCE_EDGE_THRESHOLD)
{
// The current object is closer, we suspect it is an obstacle if the previous
// reading was a border.
if (prevHint == EScanHint::Border)
{
(*it).HitInfo = EScanHint::ObstacleStart;
}
// We broke off from what we assumed was an obstacle face. But since the current
// reading is closer, we can guess the previous reading was a border, whereas the current
// reading is the beginning of a new object
else if (prevHint == EScanHint::ObstacleFace)
{
auto it_prev = it;
it_prev--;

// Mark the end of the previous obstacle
(*it_prev).HitInfo = EScanHint::Border;
(*it).HitInfo = EScanHint::ObstacleStart;
}

// Strange reading, can occur if the previous reading hit a sharp edge
// this reading could be a border edge, or the edge of an obstacle
// assume it is an obstacle edge, and defer judgment to navigational algorithm
// the current reading is assumed to be the corresponding end to the obstacle
else if (prevHint == EScanHint::ObstacleStart)
{
(*it).HitInfo = EScanHint::ObstacleEnd;
}

// The more obvious case; if we somehow know the previous point is an obstacle end,
// we expect this closer reading to be a border
else if (prevHint == EScanHint::ObstacleEnd)
{
(*it).HitInfo = EScanHint::Border;
}

// Simply assume border when in doubt...
else
{
(*it).HitInfo == EScanHint::Border;
}
}

// Readings lie on the same object, subsequent readings should be equal
else
{
// Assume both readings are borders unless identified otherwise
if (prevHint == EScanHint::Unknown)
{
auto it_prev = it;
it_prev--;

(*it_prev).HitInfo = EScanHint::Border;
(*it).HitInfo = EScanHint::Border;
}
else
{
(*it).HitInfo = prevHint;
}
}

// Process Sonar distance alone

prevHint = (*it).HitInfo;
prevTofDist = (*it).ToFDistance;

*/
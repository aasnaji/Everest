#ifndef EV_ALG_MECHARM_H
#define EV_ALG_MECHARM_H

#include "stdint.h"

namespace EVAlg
{
	class Mecharm;
}

namespace EVF
{
	class RobotCore;

	namespace Servos
	{
		class PositionServo;
	}
}

class EVAlg::Mecharm
{
public:
	Mecharm(EVF::RobotCore* core);

	void Hello();

	void ExtendDown();
	void ExtendUp();

private:
	EVF::Servos::PositionServo* m_leftArm;
	EVF::Servos::PositionServo* m_rightArm;
};
#endif // !EV_ALG_MECHARM_H

#ifndef TEST_SERVOS_DIRECTIONS_H
#define TEST_SERVOS_DIRECTIONS_H
#include "Src/Framework/EVFCore/EVFITickableObject.h"
namespace EV
{
	class RobotAI;
}

namespace EVAlg
{
	class RearDrive;
}

namespace EVAlgTests
{
	class TestServosDirection : public EVF::ITickableObject
	{
	public:
		TestServosDirection(EV::RobotAI*);
		virtual void Tick(float DeltaTime) override;
		virtual bool IsTickEnabled() override;

		
	private:
		
		bool m_started = false;
		unsigned m_time;
		EVAlg::RearDrive* m_drive;

		int m_testMode = 0;
	};
}

#endif // !TEST_SERVOS_DIRECTIONS_H


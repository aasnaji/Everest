#include "TestServosDirection.h"
#include "Src\AI\EVSRobotAI.h"
#include "Src\AI\EVSAlg\EVAlgRearDrive.h"

#include "Src\Framework\EVFUtils\EVFAssertions.h"


EVAlgTests::TestServosDirection::TestServosDirection(EV::RobotAI * ai)
{
	m_drive = ai->GetDrive();

	if (m_drive)
		m_started;
}

void EVAlgTests::TestServosDirection::Tick(float DeltaTime)
{
	if (m_started)
	{
		if (m_time < 3000 && m_testMode != 0)
		{
			m_drive->MoveForward(1);
			m_testMode = 0;
		}

		else if (m_time > 3000 && m_time < 6000 && m_testMode != 1)
		{
			m_drive->MoveBackward(1);
			m_testMode = 1;
		}

		else if (m_time > 6000 && m_time < 12000 && m_testMode != 2)
		{
			m_drive->TurnRight(90);
			m_testMode = 2;
		}

		else if (m_time > 12000 && m_time < 16000 && m_testMode != 3)
		{
			m_drive->TurnLeft(90);
			m_testMode = 3;
		}
		else
		{

		}
		m_time += DeltaTime;

		if (m_time > 20000)
			m_started = false;
	}
}

bool EVAlgTests::TestServosDirection::IsTickEnabled()
{
	return m_started;
}


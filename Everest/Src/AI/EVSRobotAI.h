#ifndef EV_ROBOT_AI
#define EV_ROBOT_AI

#include "Src/Framework/EVFCore/EVFITickableObject.h"
#include "Src/Framework/EVFMath/EVMVectors.h"

namespace EV
{
	class RobotAI;
}

namespace EVF
{
	class RobotCore;
}

namespace EVAlg
{
	class Navigation;
	class RearDrive;
	class Mecharm;
}

/*	RobotAI
*
*	The Robot AI is the construct that houses the state machine
*	that defines the robot's course of action relative to its
*	objectives
*
*	The EVRobot AI governs a drive & mechanical arm system, giving the AI
*	the ability to navigate and deploy its arm to achieve a desired task.
*/
class EV::RobotAI : public EVF::ITickableObject
{
	enum class EAIObjective : uint8_t
	{
		None,
		AlignBorder,
		AlignRamp,
		SearchRamp,
		SearchPost,
		AscendRamp,
		DescendRamp,
		Traverse,
		FollowTrajectory
	};

	// We simplify the tracking process to a 2D problem in XY coordinates.
	// The orientation of interest is on the vertical axis, the X-Axis.
	struct FAITarget
	{
		FVector Position2D;
		float Orientation;	// Yaw
	};

	struct FAICache
	{
		float Yaw; // Radians
		float HitDistanceCm;	// Distance in cm last acquired from navigation

		void Clear()
		{
			Yaw = 0; HitDistanceCm = 0;
		}
	};

public:
	RobotAI(EVF::RobotCore* core);
	~RobotAI();

	/* ITickableObject Interface*/
	virtual void Tick(float DeltaTime) override;
	virtual bool IsTickEnabled() override;

	EVAlg::Navigation* GetNavigation() const;
	EVAlg::Mecharm* GetMecharm() const;
	EVAlg::RearDrive* GetDrive() const;

	/*	Begin Objective
	*
	*	Sets the AI state's to achieve a desired AI Objective. The state
	*	transition induces changes to the AI's target of interest, and its
	*	usage of its components. The AI also replaces existing callbacks to
	*	match its current state. 
	*
	*	i.e: A robot in ramp detection state will respond to a rotation complete
	*		callback by calling a transition to the next state; whereas by default,
	*		a rotation completetion stands by for the AI's decision based on existing
	*		objectives and navigation hints
	*/
	void BeginObjective(EAIObjective);

private:
	/*	Certain aspect of the AI operations rely on sequencing events,
	*	to that effect, certain functions execute with an argument dictating
	*	what method to exectue after completion as a form of event sequencing
	*/
	typedef void(RobotAI::*Invokable)();
	
	/*	Process a request to align the robot to the ramp. If rotation and alignment
	*	are successful, the onSuccess method is invoked afterwards. Otherwise, if
	*	the instruction was invalid or failed to perform, invoke the onFail method.
	*/
	void AlignToRamp(Invokable onFail);


	/*	On Begin Slide
	*
	*	Executed when the AI is in the "DescendRamp" state.
	*	Ensures Drive is stopped to initiate a power-slide sequence.
	*	The state of the AI is suspended until the power-slide is complete
	*	as determined by the RAMP_SLIDE_DURATION_MS time in millis.
	*
	*
	*	On End Slide
	*	
	*	Executed after the power-slide is completed. The AI rests in place
	*	to ensure the motion is fully complete. The AI performs a scan to
	*	determine its orientation relative to the boundary, from which it can
	*	orient itself to face the remainder of the course. The state is then 
	*	transitioned to "SearchPost" to carry out the detection accordingly
	*/
	void OnBeginSlide();
	void OnEndSlide();

	void OnLocationReached() const;
	void OnRotationComplete() const;

	void OnRampDetected();
	void OnPostDetected();

private:
	/* Cannot be copied, assigned, or compared */
	RobotAI(RobotAI&&) = delete;
	RobotAI(const RobotAI&) = delete;
	void operator =(const RobotAI&) = delete;

	void SetupCallbacks();

	// Watchdog timers are continously accumulating unless the robot is able
	// to find targettable objective. In case the watchdog timer exceeds a set
	// duration, WhereAmI is called to re-assess the status of the AI and objectives
	void WhereAmI();

	inline void ClearWatchDog()
	{
		m_watchdogTimer = 0;
	}

private:
	EVF::RobotCore* m_robotCore;

	EVAlg::Navigation* m_navAlg;
	EVAlg::RearDrive* m_driveAlg;
	EVAlg::Mecharm* m_armAlg;

	mutable EAIObjective m_aiObjective;
	mutable FAITarget* m_aiTarget;
	mutable FAICache m_aiCache;

	unsigned long m_watchdogTimer;
};

#endif // !EV_ROBOT_AI

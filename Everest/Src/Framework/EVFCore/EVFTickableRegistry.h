/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#ifndef EVF_TICKABLE_REGISTRY_H
#define EVF_TICKABLE_REGISTRY_H

#include "Src/Framework/EVFSupport/Containers/EVFList.h"
namespace EVF
{
	class TickableRegistry;
	class ITickableObject;
}

/*	Tickable Registry
*
*	This singleton class keeps track of every registered Tickable Object.
*	With every update cycle, call the associated Tick method. The Tickable
*   registry uses a timer to guage the time elapsed since the last update cycle
*/
class EVF::TickableRegistry
{

public:
	void ApplyTick();
	
public:
	TickableRegistry();
	~TickableRegistry();

	// Adds the tickable object to the managed registry
	void RegisterTickableObject(ITickableObject* tickableObject);

	void PostInitializeRegistry();

private:
	// Represents the registry of objects
	TList<ITickableObject*> m_tickableObjects; 
	
	// Time in [ms] of the last recorded update cycle
	unsigned long m_LastUpdateTime;
};

#endif // !EVFC_TICKABLE_REGISTRY
/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#include "EVFPositionServo.h"
#include "Src/Framework/EVFMath/EVMFunctions.h"

#include "Src/Framework/EVFUtils/EVFLogger.h"

#include "Arduino.h"
#include "Print.h"

using namespace EVF::Servos;

/*	Define maximum values with some margins to avoid
*	jamming, vibrations, and other unwanted behaviours
*/
#define DEFAULT_ANGLE			90
#define MIN_ANGLE				15
#define MAX_ANGLE				155
#define DELTA					100	// Time for 1 ms degree increment 

PositionServo::PositionServo(uint8_t pinNumber, bool mirrored)
	: IBaseServo::IBaseServo(pinNumber, mirrored)
{
	//OnReset();
}

PositionServo::~PositionServo()
{
}

void PositionServo::SetAngle(int degrees)
{
	
	m_angle = DEFAULT_ANGLE+(!m_mirrored ? degrees : 180-degrees);

	servo->write(FMath::Clamp(MIN_ANGLE, MAX_ANGLE, m_angle));
	delay(10);
}

void PositionServo::IncrementAngle(int deltaDegrees) 
{
	// For safety, prevent user from inputing large dtheta
	deltaDegrees = FMath::Clamp(-5, 5, deltaDegrees);

	// If our axis is mirrored, an increment corresponds to an inverse operation - decrement.
	m_angle = !m_mirrored ? m_angle + deltaDegrees : m_angle - deltaDegrees;

	// Apply the resulting absolute rotation
	servo->write(m_angle); 
	delay(10);
}

uint16_t PositionServo::GetAngle() const
{
	return  m_angle;
}

void PositionServo::OnStop()
{
	// If the servo was attempting to rotate to a target value, 
	// write our current value so that the transition ends abruptly
	servo->write(m_angle);
}

void PositionServo::OnReset()
{
	uint8_t angle = !m_mirrored ? DEFAULT_ANGLE: DEFAULT_ANGLE;
	SetAngle(angle);
}

/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#ifndef EVF_IBASE_SERVO_H
#define EVF_IBASE_SERVO_H

#include "Src/Framework/EVFDispatch/EVFDelegate.h"
#include "Src/Framework/EVFCore/Servos/EVFServoProfiles.h"
#include "Servo.h"
#include "Arduino.h"
//#include "Src/Framework/EVFCore/Sensors/EVFSensorCurrent.h"

namespace EVF
{
	namespace Servos
	{
		class IBaseServo;
		class IServoProfile;
	}
}

class EVF::Servos::IBaseServo
{
	
public:
	IBaseServo(uint8_t PinNumber, bool mirrored = false)
		: m_pinNumber (PinNumber)
		, m_stopCb (nullptr)
		, m_resetCb (nullptr)
		, m_mirrored (mirrored)
	{
		servo = new Servo();
		servo->attach(PinNumber);
	}

	virtual ~IBaseServo() 
	{
		servo->detach();

		delete servo;
		servo = nullptr;

	}

	// Immediately halts the operation of the motor by setting it to the idle speed.
	// Executes the Stop Callback if available
	void Stop() 
	{ 
		OnStop();

		if(m_stopCb)
		m_stopCb();
	}

	// Resets the motor to a predefined default operating state
	// Executes the Reset Callback if available
	void Reset() 
	{ 
		OnReset();

		if(m_resetCb)
		m_resetCb;
	}

	/*	typedef void(*StopCallback)()
	*	The signature of the callback function to be invoked
	*	when the servo is issued a stop command.
	*/
	typedef void(*StopCallback)();
	void SetStopCallback(StopCallback callback)
	{
		m_stopCb = callback;
	}

	/*	typedef void(*ResetCallback)()
	*	The signature of the callback function to be invoked
	*	when the servo is issued a reset command.
	*/
	typedef void(*ResetCallback)();
	void SetResetCallback(ResetCallback callback)
	{
		m_resetCb = callback;
	}

	uint8_t GetPinNumber() const
	{
		return m_pinNumber;
	}

	/*	Gives application programmers the ability to override default
	*	calibration parameters of the servo by applying specific profiles
	*	tailored to specific applications.
	*
	*	Servo Profiles offer means to easily change the behaviour of the servo
	*	during runtime. For example, a drive servo might need different calibration
	*	values if it is operating on flat surface versus an inclined plane. To take 
	*	that into account, an inclined plane profile can be swapped in instead of the
	*	flat plane profile.
	*/
	
	template<typename ProfileClass>
	void ApplyServoProfile()
	{
		ProfileClass* profile = new ProfileClass();
		
		if (profile->ApplyToServo(this))
		{
			Serial.println("Servo Profile Updated Successfully");
		}
		else
		{
			Serial.println("Failed applying Servo Profile...");
		}

		delete profile;
	}
	
	bool IsValid()
	{
		if (!servo)
			return false;
		return servo->attached();
	}

protected:
	virtual void OnStop() = 0;
	virtual void OnReset() = 0;

private:

protected:
	Servo* servo;
	bool m_mirrored;	// I really hate having to do this...
	float m_stallCurrent;

private:
	StopCallback m_stopCb = nullptr;
	ResetCallback m_resetCb = nullptr;

	uint8_t m_pinNumber;
};
#endif // !EVF_IBASE_SERVO_H

/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#ifndef EVF_CONTINOUS_SERVO_H
#define EVF_CONTINOUS_SERVO_H

#include "Servo.h"
#include "EVFIBaseServo.h"

namespace EVF { namespace Servos {

	class ContinousServo;

	enum class EMoveDirections : int8_t
	{
		Forward = 1,
		Backward = -1
	};
}}

/*	Continous Servos
*
*	Offers calibrated method to control our continous servo motor
*	via the Arduino's servo interface. Servo is controlled explicitly
*	by setting percentage speed values.
*/
class EVF::Servos::ContinousServo : public EVF::Servos::IBaseServo
{
	/* Allow the profile classes to internally override our parameters if needed */
	friend class ContServoLProfile;
	friend class ContServoRProfile;
public:
	ContinousServo(uint8_t pinNumber, bool mirrored = false);
	~ContinousServo();

	void SetSpeed(float percent, EMoveDirections servoDirection = EMoveDirections::Forward);
	float GetSpeed() const;
	float GetSpeedRad() const;

protected:
	virtual void OnStop() override final;
	virtual void OnReset() override final;

private:
	float m_percentSpeed;

	uint16_t m_idleVal;
	uint16_t m_maxVal;
	uint16_t m_minVal;
};

#endif // !EVF_CONTINOUS_SERVO_H

/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#ifndef EVF_POSITION_SERVO_H
#define EVF_POSITION_SERVO_H

#include "Servo.h"
#include "EVFIBaseServo.h"

namespace EVF { namespace Servos {
	class PositionServo;
}}

/*	Position Servo
*
*	Offers calibrated methods to control our angle-based positional servo
*	via the Arduino's servo library. The servo is controlled by explicitly
*	setting a desired angle value relative to a calibrated value
*/
class EVF::Servos::PositionServo : public EVF::Servos::IBaseServo
{
public:
	PositionServo(uint8_t pinNumber, bool mirrored = false);
	~PositionServo();

	/*	Set Angle
	*
	*	Orients the position servo to an absolute angle.
	*	SetAngle has no limit on the extent of the swing,
	*	it can be used to perform large rotations.
	*
	*	Use with caution!
	*	See IncrementAngle for alternative rotation method
	*
	*	@Param int degrees:	The absolute angle in degrees to be set
	*/
	void SetAngle(int degrees);

	/*	Increment Angle
	*
	*	Adds relative rotation to the servo's current orientation.
	*	Incremental rotations are clamped between to a magnitude of
	*	5 degrees for safety. 
	*
	*	See SetAngle for alternative rotation method
	*
	*	@Param int deltaDegrees: The relative rotation to be added
	*/
	void IncrementAngle(int deltaDegrees);

	/*	Get Angle
	*
	*	Returns the current absolute rotation of the servo in degrees
	*/
	uint16_t GetAngle() const;

protected:
	virtual void OnStop() override final;
	virtual void OnReset() override final;
private:
	uint8_t m_angle;
};
#endif // !EVF_POSITION_SERVO_H

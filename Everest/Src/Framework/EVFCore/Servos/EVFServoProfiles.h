#ifndef EVF_SERVO_PROFILES_H
#define EVF_SERVO_PROFILES_H

namespace EVF
{
	namespace Servos
	{
		class IBaseServo;

		class IServoProfile;
		class ContServoLProfile;
		class ContServoRProfile;
	}
}
	/*	IServoProfile
	*
	*	Represent a setting profile to be applied to servos.
	*	The job of a servo profile is to manipulate an existing
	*	servo object and apply custom calibration settings or biases
	*	for specific applications.
	*
	*	A Servo Profile implements ApplyToServo; which has full freedom
	*	on which parameters of the servo to modify.
	*/
	
	class EVF::Servos::IServoProfile
	{
	public:
		virtual ~IServoProfile();
		virtual bool ApplyToServo(IBaseServo* servo) = 0;
	protected:
		IServoProfile();
	};


	class EVF::Servos::ContServoLProfile : public EVF::Servos::IServoProfile
	{
	public:
		ContServoLProfile();
		~ContServoLProfile();

		virtual bool ApplyToServo(IBaseServo* servo) override;
	};

	class EVF::Servos::ContServoRProfile : public EVF::Servos::IServoProfile
	{
	public:
		ContServoRProfile();
		~ContServoRProfile();

		virtual bool ApplyToServo(IBaseServo* servo) override;
	};



#endif // !EVF_SERO_PROFILES_H


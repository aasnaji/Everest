#include "EVFServoProfiles.h"
#include "EVFContinousServo.h"
#include "EVFPositionServo.h"

using namespace EVF::Servos;

EVF::Servos::IServoProfile::~IServoProfile()
{
}

EVF::Servos::IServoProfile::IServoProfile()
{
}



/* Continous Servo - Left Drive Profile */
EVF::Servos::ContServoLProfile::ContServoLProfile()
{
}

EVF::Servos::ContServoLProfile::~ContServoLProfile()
{
}

bool EVF::Servos::ContServoLProfile::ApplyToServo(IBaseServo* servo)
{
	ContinousServo* _servo = (ContinousServo*)(servo);
	if ( _servo )
	{
		_servo->m_idleVal = 95;
		_servo->m_maxVal = 101;
		_servo->m_minVal = 88;
		_servo->m_mirrored = false;
		return true;
	}
	else
	{
		return false;
	}
}



/* Continous Servo - Right Drive Profile */
EVF::Servos::ContServoRProfile::ContServoRProfile()
{
}

EVF::Servos::ContServoRProfile::~ContServoRProfile()
{
}

bool EVF::Servos::ContServoRProfile::ApplyToServo(IBaseServo * servo)
{
	ContinousServo* _servo = ((ContinousServo*)servo);
	if ( _servo )
	{
		_servo->m_idleVal = 94;
		_servo->m_maxVal = 88;
		_servo->m_minVal = 101;
		_servo->m_mirrored = false;

		return true;
	}
	else
	{
		return false;
	}
}

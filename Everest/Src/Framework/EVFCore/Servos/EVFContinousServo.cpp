/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#include "EVFContinousServo.h"
#include "EVFServoProfiles.h"
#include "Src/Framework/EVFMath/EVMMath.h"

#include "Src/Framework/EVFUtils/EVFAssertions.h"
#include "Src/Framework/EVFUtils/EVFLogger.h"

#include "Print.h"
#include "Arduino.h"

using namespace EVF::Servos;

#define	IDLE_PWM_MICRO		1518

#define MAX_PWM_MICRO		1588
#define MIN_PWM_MICRO		1448

#define ABS(x)				( x ^ ( x >> sizeof(x)*8-1) - (x >> sizeof(x)*8-1) )

ContinousServo::ContinousServo(uint8_t pinNumber, bool mirrored)
	: IBaseServo::IBaseServo (pinNumber, mirrored)
	, m_percentSpeed (0)
	, m_idleVal(IDLE_PWM_MICRO)
	, m_minVal(MIN_PWM_MICRO)
	, m_maxVal(MAX_PWM_MICRO)
{
	check(servo->attached());
	OnReset();
}

ContinousServo::~ContinousServo()
{
	// Halt servo for safety
	OnStop();
}

void ContinousServo::SetSpeed(float percent, EMoveDirections servoDirection)
{
	// So that if our conditional magically fails, the motor remains idle.
	uint16_t lerpTo = m_idleVal;

	/*	Fool-proof way getting the right direction, compiler should optimize this away! */
	if (servoDirection == EMoveDirections::Forward)
	{
		// If our direction is forward, but the servo is in a mirrored convention, 
		// we need to actually move backward to achieve desired results
		lerpTo = m_mirrored ? m_minVal : m_maxVal;
	}
	else
	{
		// Conversely, if moving backward with a servo in a mirrored convetion, 
		// we need to actually move forward to achieve desired results
		lerpTo = m_mirrored ? m_maxVal : m_minVal;
	}

	/* Linearly interpolates values for PWM/Angle between the min and max established values
	 *
	 * The mirrored flag reverses our direction convention. This ensure conventional direction
	 * is retained in the application side without concerns about orientation of the servo
	 */
	uint16_t PWM = FMath::Lerp(m_idleVal
							, lerpTo
							, FMath::Clamp(0, 1, percent));

	// Apply the PWM or Angle value
	servo->write(PWM);
	m_percentSpeed = percent;
}

float ContinousServo::GetSpeed() const
{
	return m_percentSpeed;
}

float ContinousServo::GetSpeedRad() const
{
	return 0.0f;
}

void ContinousServo::OnStop()
{
	SetSpeed(0);
}

void ContinousServo::OnReset()
{
	SetSpeed(0);
}

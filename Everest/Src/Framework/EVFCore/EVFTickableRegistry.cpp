/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/
#include "EVFTickableRegistry.h"
#include "EVFITickableObject.h"

#include "Src/Framework/EVFUtils/EVFAssertions.h"
#include "Src/Framework/EVFUtils/EVFLogger.h"

#include "Arduino.h"

#include "stdio.h"

using namespace EVF;

TickableRegistry::TickableRegistry()
	: m_LastUpdateTime(0)
{
	// This LOG causes system hang up!
	//LOG("Hello From Tick Registry!");
}

TickableRegistry::~TickableRegistry()
{
}

void TickableRegistry::ApplyTick()
{
	// Calculate the time elapsed since the last update cycle
	unsigned long delta = 0;

	if (m_tickableObjects.Size() == 0)
		return;

	TListIterator<ITickableObject*> It = m_tickableObjects.Begin();

	// Iterate over the tickable objects and invoke their Tick method
	// if they are enabled
	for (; It != m_tickableObjects.End(); It++)
	{
		delta = millis() - m_LastUpdateTime;
		
		if (It->IsTickEnabled())
			It->Tick((float)delta);
	}

	m_LastUpdateTime = millis();
}

// Adds a tickable object to the registry, allowing it to be ticked every update cycle
void TickableRegistry::RegisterTickableObject(ITickableObject * tickableObject)
{
	if (m_tickableObjects.Contains(tickableObject))
	{
		// Adding the same object twice is an indication of something going wrong...assert on re-entry
		check(false);
		return;
	}

	m_tickableObjects.Insert(tickableObject);
}

void TickableRegistry::PostInitializeRegistry()
{
	m_LastUpdateTime = millis();
}



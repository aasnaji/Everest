#include "EVFITickableObject.h"
#include "EVFTickableRegistry.h"
#include "Src/Framework/EVFRobotCore.h"

using namespace EVF;

// Tickable objects register automatically via the robot's core system.
// This is not ideal, but the current architecture does not allow me to
// assume a TickableRegistry exist statically. The only workaround is to
// enforce users of sub-classes to pass an owning TickableRegistry as argument.
ITickableObject::ITickableObject()
{
	ev_RobotCore->HandleTickableRegistration(this);
}

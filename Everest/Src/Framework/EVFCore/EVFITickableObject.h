/**************************************************************************
 * Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
 *
 * This software is licensed under the MTE Public License v1.0
 * which bares no legal significance whatsoever.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#ifndef EVF_ITICKABLE_OBJECT_H
#define EVF_ITICKABLE_OBJECT_H
#include "Arduino.h"
#include "Print.h"

namespace EVF
{
	class ITickableObject;
	class TickableRegistry;
}

/*	ITickableObject
 *
 *	An interface to be implemented by any class that requires ticking functionalities.
 *  Tickable objects are registered by the TickableRegistry, which continously invokes
 *  the Tick method for every registered object. Sub-classes would want to implement 
 *  their logic by overriding the Tick method.
*/
class EVF::ITickableObject
{
public:
	// Default constructor -- Automatically register a tickable object with the registry
	ITickableObject();

	// As this is an interface, the destructor must be virtual
	virtual ~ITickableObject()
	{
	}

	/*	Tick
	*	
	*	The Tick method is called on this object during update cycle.
	*	Implement logic here.
	*
	*	@Param float DeltaTime:	The time elapsed between the current and previous Tick
	*/
	virtual void Tick(float DeltaTime) = 0;

	/*	IsTickEnabled
	*
	*	If true, the object can be Ticked every update cycle.
	*	Otherwise, it is ignored.
	*/
	virtual bool IsTickEnabled() = 0;
};

#endif // !EVFC_ITICKABLE_OBJECT
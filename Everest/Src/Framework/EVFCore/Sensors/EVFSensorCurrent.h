/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#ifndef EVF_SENSOR_CURRENT_H
#define EVF_SENSOR_CURRENT_H

#include "EVFSensor.h"

namespace EVF 
{ 
	namespace Sensors 
	{
		class SensorCurrent;
	} 

	namespace Servos
	{
		class IBaseServo;
	}
}

/*	Sensor Current
*
*	This sensor provides an interface that handles a current sensing
*	circuit or sensor.
*/
class EVF::Sensors::SensorCurrent : public EVF::Sensors::Sensor
{
public:
	/*	Constructor that accepts the read pin location of the sensor
	*	and the specified max allowable current draw. No user-defined
	*	current is monitored by default.
	*/
	SensorCurrent(uint8_t pinNumber, float maxCurrent = 1);

	/*	Constructor that additionally accepts a user defined monitor current.
	*	The sensor notifies listener when both the max current is exceeded, and
	*	when a monitor current is achieved
	*/
	SensorCurrent(uint8_t pinNumber, float monitorCurrent , float maxCurrent = 1);

	/* ITickable Interface */
	virtual void Tick(float DeltaTime) override;

	float GetAverage() const;

	/*	Function signature for callback functions invoked
	*	when the sensor detects a maximum over-current condition
	*/
	typedef void(*MaxCurrentCallback)(uint8_t, float);
	void SetMaxCurrentCallback(MaxCurrentCallback);

	/*	Function signature for callback functions invoked
	*	when a sensor detects any of the user-defined amperage
	*/
	typedef void(*MonitorCurrentCallback)(uint8_t, float);
	void SetMonitorCurrentCallback(MonitorCurrentCallback);

	
	void SetMonitorCurrent(float current);

private:
	void OnMaxCurrent(float);
	void OnMonitorCurrent(float);

private:
	float m_maxCurrent;
	float m_monitorCurrent;
	FSensorBuffer<float> m_buffer;

	MaxCurrentCallback m_monitorCurrentCb;
	MonitorCurrentCallback m_maxCurrentCb;
};
#endif // !EVF_SENSOR_CURRENT_H


/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#ifndef EVF_SENSOR_SONAR_H
#define EVF_SENSOR_SONAR_H

#include "EVFSensor.h"

#include "Src/Framework/EVFDispatch/EVFDelegate.h"

namespace EVF { namespace Sensors {

		class SensorSonar;
}}

class EVF::Sensors::SensorSonar : public EVF::Sensors::Sensor
{
	struct FSonarHeader
	{
		uint8_t TriggerPin : 4;
		uint8_t EchoPin : 4;
		uint8_t EchoTimeoutDistance : 8;		// The maximum distance in cm the ultrasonic can detect
	};


	// The callback function the sonar uses to report a finished reading
	typedef void(*SonarEchoCallback)(float);

public:
	SensorSonar(uint8_t triggerPin, uint8_t echoPin);
	~SensorSonar();

	/* ITickableObject Interface */
	virtual void Tick(float DeltaTime) override final;	// Implement core sensor logic here

	/*	An Alternative Mode of operation of ultrasonic, specifically
	*	due to its heavy time budget.
	*
	*	Acquire a distance reading on a per-request basis.
	*
	*	@Return float: This distance measurement in cm
	*/
	float RequestMeasurement();

	void SetMaxRange(uint8_t distance_cm);
	uint8_t GetMaxRange() const;

	inline static unsigned EchoTimeoutMicro()
	{
		return (255/ 0.034);	// 0.034 cm/us * 255 cm yields a timeout such that a distance greater than 255 is invalidated
	}

	inline static unsigned EchoTimeoutMs()
	{
		return (255 / 0.034) / 1000;
	}
		

private:

	FSonarHeader m_sonarParams;

	FDelegate m_OnDistanceUpdated;
};
#endif // !EVF_SENSOR_SONAR_H

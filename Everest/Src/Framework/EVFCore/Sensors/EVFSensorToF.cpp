/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#include "EVFSensorToF.h"
#include "Src/Libraries/AdafruitVL53L0/Adafruit_VL53L0X.h"
#include "Src/Framework/EVFUtils/EVFAssertions.h"
#include "Src/Framework/EVFMath/EVMFunctions.h"

#define TOF_COLLISION_THRESHOLD					100		//mm
#define TOF_OBSTACLE_THRESHOLD					200

using namespace EVF::Sensors;

SensorToF::SensorToF()
	: Sensor(PIN_UNDEFINED, ESensorType::ToF)
	, m_mode(EToFModes::DefaultMode)
	, m_obstacleCb(nullptr)
	, m_collisionEnterCb(nullptr)
	, m_collisionExitCb(nullptr)
{
	m_tofSensor = new Adafruit_VL53L0X();

	check(m_tofSensor);
	m_tofSensor->begin();
	delay(10);

	// Calibrate the buffer of the sensor
	VL53L0X_RangingMeasurementData_t measurement;
	m_tofSensor->getSingleRangingMeasurement(&measurement);

	FSensorReading<uint16_t> readings;
	readings.TimeStamp = measurement.TimeStamp;
	readings.Reading = measurement.RangeMilliMeter;

	for (int i = 0; i < 4; i++)
	{
		m_buffer.WriteReading(readings);
	}
}

SensorToF::~SensorToF()
{
	delete m_tofSensor;
	m_tofSensor = nullptr;
}

void SensorToF::Tick(float DeltaTime)
{
	VL53L0X_RangingMeasurementData_t measurement;
	
	VL53L0X_Error err =  m_tofSensor->getSingleRangingMeasurement(&measurement);

	// Populate a reading in our buffer
	FSensorReading<uint16_t> reading;
	uint16_t distance_mm = 0;

	// The data is a single integer
	reading.TimeStamp = measurement.TimeStamp;

	// If no error is detected, populate with the new reading.
	// Otherwise, fill the missed reading with the previously processed reading
	if (err == VL53L0X_ERROR_NONE)
	{
		distance_mm = measurement.RangeMilliMeter;
	}
	else
	{
		distance_mm = m_buffer.GetReading().Reading;
	}
	
	// Store the distance value to be written to the buffer
	reading.Reading = distance_mm;
	Serial.println(distance_mm);

	// Sharp reading transition, signify obstacle detection
	// Note: We need to cast to an int, because the subraction can cause an overflow, and false detection
	if ((int)m_buffer.GetReading().Reading - (int)distance_mm >= TOF_OBSTACLE_THRESHOLD && !(m_detectionFlags & 1 << 1))
	{
		// Turn on Obstacle Flag
		m_detectionFlags |= 1 << 1;
		if (m_obstacleCb)
			m_obstacleCb(distance_mm);
	}
	else if( (int)distance_mm - (int) m_buffer.GetReading().Reading > TOF_OBSTACLE_THRESHOLD && (m_detectionFlags & 1 << 1))
	{
		// Turn off Obstacle Flag
		m_detectionFlags &= ~(1 << 1);
	}

	// Entered Collision Range
	if (distance_mm <= TOF_COLLISION_THRESHOLD && m_buffer.GetReading().Reading > TOF_COLLISION_THRESHOLD && !(m_detectionFlags & 1 << 2))
	{
		// Turn on collision flag
		m_detectionFlags |= 1 << 2;
		if (m_collisionEnterCb)
			m_collisionEnterCb(distance_mm);
	}
	// Exited Collision Range
	else if (distance_mm > TOF_COLLISION_THRESHOLD && m_buffer.GetReading().Reading < TOF_COLLISION_THRESHOLD && (m_detectionFlags & 1 << 2))
	{
		// Turn off collision flag
		m_detectionFlags &= ~(1 << 2);
		if (m_collisionExitCb)
			m_collisionExitCb(distance_mm);
	}


	// Finally, write to managed buffer
	m_buffer.WriteReading(reading);
}

bool SensorToF::SetToFMode(EToFModes mode)
{
	m_mode = mode;

	m_tofSensor->setRange(static_cast<uint8_t>(m_mode));
	Serial.println("ToF mode changed");
}

FSensorReading<uint16_t> SensorToF::GetCurrReading()
{
	return m_buffer.GetReading();
}

uint16_t SensorToF::GetCurrDistance()
{
	return m_buffer.GetReading().Reading;
}



void SensorToF::SetObstacleCallback(ObstacleCallback cb)
{
	m_obstacleCb = cb;
}

void SensorToF::SetCollisionEnterCallback(CollisionEnter cb)
{
	m_collisionEnterCb = cb;
}

void SensorToF::SetCollisionExitCallback(CollisionExit cb)
{
	m_collisionExitCb = cb;
}


/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#include "EVFSensorCurrent.h"

using namespace EVF::Sensors;

// We assume all values of current are positive. As such, a negative value indicates a "null" or "undefined" state

SensorCurrent::SensorCurrent(uint8_t pinNumber, float maxCurrent)
	: Sensor(pinNumber, ESensorType::CurrentSense)
	, m_maxCurrent(maxCurrent)
	, m_monitorCurrent(-1)
	, m_maxCurrentCb(nullptr)
	, m_monitorCurrentCb(nullptr)
{
	pinMode(pinNumber, INPUT);
}

SensorCurrent::SensorCurrent(uint8_t pinNumber, float monitorCurrent, float maxCurrent)
	: Sensor(pinNumber, ESensorType::CurrentSense)
	, m_maxCurrent(maxCurrent)
	, m_monitorCurrent(monitorCurrent)
	, m_maxCurrentCb(nullptr)
	, m_monitorCurrentCb(nullptr)
{
	pinMode(pinNumber, INPUT);
}

void SensorCurrent::Tick(float DeltaTime)
{
}

float SensorCurrent::GetAverage() const
{
	FSensorReading<float> readings[4];
	m_buffer.GetReadings(readings);

	return ((readings[0].Reading + readings[1].Reading + readings[2].Reading + readings[3].Reading) / 4);
}

void EVF::Sensors::SensorCurrent::SetMaxCurrentCallback(MaxCurrentCallback cb)
{
	m_maxCurrentCb = cb;
}

void SensorCurrent::SetMonitorCurrentCallback(MonitorCurrentCallback cb)
{
	m_monitorCurrentCb = cb;
}

void SensorCurrent::SetMonitorCurrent(float current)
{
	m_monitorCurrent = current;
}

void SensorCurrent::OnMaxCurrent(float val)
{
	if (m_maxCurrentCb)
		(m_maxCurrentCb)(m_pinNumber, val);
}

void SensorCurrent::OnMonitorCurrent(float val)
{
	if (m_monitorCurrentCb)
		(m_monitorCurrentCb)(m_pinNumber ,val);
}

/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#include "EVFSensorLimit.h"
#include "Arduino.h"

#include "Src/Framework/EVFUtils/EVFAssertions.h"
using namespace EVF::Sensors;

EVF::Sensors::SensorLimit::SensorLimit(uint8_t pinNumber)
	: Sensor(pinNumber, ESensorType::LimitSwitch)
{
	// Limit switches by definition are read only pins, set the input mode accordingly
	pinMode(pinNumber, INPUT);
}

EVF::Sensors::SensorLimit::~SensorLimit()
{
}

void EVF::Sensors::SensorLimit::Tick(float DeltaTime)
{
	// DIGITAL HIGH = 0x1 (true)
	// DIGITAL LOW = 0x0 (false)
	bool readVal = digitalRead(m_pinNumber);

	// If the value has changed, update it and notify any listeners
	if (m_value != readVal)
	{
		m_value = readVal;
		OnLimitSwitchStateChanged(readVal);
	}
}

void SensorLimit::OnLimitSwitchStateChanged(bool triggered)
{
}


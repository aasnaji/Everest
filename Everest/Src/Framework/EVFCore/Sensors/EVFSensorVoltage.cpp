#include "EVFSensorVoltage.h"

using namespace EVF::Sensors;

SensorVoltage::SensorVoltage(uint8_t pinNumber)
	: Sensor(pinNumber, ESensorType::VoltageSense)
{
}

SensorVoltage::~SensorVoltage()
{
}

void SensorVoltage::Tick(float DeltaTime)
{
}

/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#include "EVFSensorSonar.h"

#include "Arduino.h"

using namespace EVF::Sensors;

#define TRIGGER_HOLD_TIME					0.01						// 0.01 ms indicated 10 micros needed for trigger to generated pulses	
#define ECHO_TIMEOUT_MICRO					(m_sonarParams.EchoTimeoutDistance/0.034)	// 0.034 cm/us * 255 cm yields a timeout such that a distance greater than 255 is invalidated
#define ECHO_TO_DIST(PULSEWIDTH)			(PULSEWIDTH * (0.034/2))	// Converts an echo pulse reading to distance measurement in cm

SensorSonar::SensorSonar(uint8_t triggerPin, uint8_t echoPin)
	: Sensor(triggerPin, ESensorType::Sonar)
{
	// Set Pin Modes
	pinMode(triggerPin, OUTPUT);
	pinMode(echoPin, INPUT);

	m_sonarParams.EchoPin = echoPin;
	m_sonarParams.TriggerPin = triggerPin;
	m_sonarParams.EchoTimeoutDistance = 250; // cm
}

SensorSonar::~SensorSonar()
{
}

void SensorSonar::Tick(float DeltaTime)
{
	/*
	digitalWrite(m_sonarParams.TriggerPin, HIGH);
	if (m_sonarParams.TriggerHeldTime/1000.0 <= TRIGGER_HOLD_TIME)
	{
		m_sonarParams.TriggerHeldTime += DeltaTime;
		return;
	}
	digitalWrite(m_sonarParams.TriggerPin, LOW);

	// TRIGGER HOLD TIME surpassed
	// The pulses are now generated!
	auto duration = pulseIn(m_sonarParams.EchoPin, HIGH, ECHO_TIMEOUT_MICRO);

	// Cache our most recent distance readings
	uint8_t distance = ECHO_TO_DIST(duration);
	m_sonarParams.LastDistMeasured = distance;
	
	// Broadcast the event to all listeners
	m_OnDistanceUpdated.Broadcast(&distance);

	// Auto-Disable if it is a one-time query
	if (m_sonarParams.QueryOnce)
		SetSensorEnabled(false);
	
	// Reset
	m_sonarParams.TriggerHeldTime = 0;
	digitalWrite(m_sonarParams.TriggerPin, LOW);
	delayMicroseconds(2);
	*/
}

float SensorSonar::RequestMeasurement()
{
	// Reset Trigger
	digitalWrite(m_sonarParams.TriggerPin, LOW);
	delayMicroseconds(2);

	// Apply 10 us trigger
	digitalWrite(m_sonarParams.TriggerPin, HIGH);
	delayMicroseconds(TRIGGER_HOLD_TIME);
	digitalWrite(m_sonarParams.TriggerPin, LOW);

	auto duration = pulseIn(m_sonarParams.EchoPin, HIGH, ECHO_TIMEOUT_MICRO);
	float distance = ECHO_TO_DIST(duration);

	return distance;
}

void SensorSonar::SetMaxRange(uint8_t distance_cm)
{
	m_sonarParams.EchoTimeoutDistance = distance_cm;
}

uint8_t SensorSonar::GetMaxRange() const
{
	return m_sonarParams.EchoTimeoutDistance;
}

/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#ifndef EVF_SENSOR_LIMIT_H
#define EVF_SENSOR_LIMIT_h

#include "EVFSensor.h"

namespace EVF { namespace Sensors {

		class SensorLimit;
}}

/*	Function signatures for events associated with limit switches
*/
typedef void(*LimSwitchStateChangedCallback)(bool);

class EVF::Sensors::SensorLimit : public EVF::Sensors::Sensor
{
public:
	SensorLimit(uint8_t pinNumber);
	~SensorLimit();

	/* ITickableObject Interface */
	virtual void Tick(float DeltaTime) override final;	// Implement core sensor logic here

	/* Events & Delegates */
	void OnLimitSwitchStateChanged(bool triggered);

private:
	uint8_t m_value;
};

#endif // !EVF_SENSOR_LIMIT_H



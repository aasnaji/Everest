/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#ifndef EVF_SENSOR_TOF_H
#define EVF_SENSOR_TOF_H

#include "EVFSensor.h"

namespace EVF{ namespace Sensors{

	class SensorToF;
}}

class Adafruit_VL53L0X;
/*	Sensor ToF
*
*	The ToF sensor class acts as a wrapper around the VL53L0X lidar sensor.
*	The sensor stores a buffer of individual distance measurements.
*
*	The ToF sensor acts as a bottle neck to the system as its speed budget
*	is in the millisecond scale - well below our clocking speed of 16 MHz
*
*	Performance Profiles:
*
*	/ MODE				/ TIME BUDGET
*	Default Mode			30ms
*	High Accuracy			200ms
*	Long Range				33ms
*	High Speed				20ms
*/
class EVF::Sensors::SensorToF : public EVF::Sensors::Sensor
{
public:

	enum class EToFModes : uint8_t
	{
		DefaultMode = 0,
		HighAccuracy,
		LongRange,
		HighSpeed
	};

	SensorToF();
	~SensorToF();

	/* ITickableObject Interface */
	virtual void Tick(float DeltaTime) override final;	// Implement core sensor logic here

	bool SetToFMode(EToFModes mode);

	/*	Returns the most recent sensor reading
	*	which includes a time stamp and the distance in mm.
	*/
	FSensorReading<uint16_t> GetCurrReading();

	/*	Returns only the most recent distance reading in mm
	*/
	uint16_t GetCurrDistance();

	/* Function Signature for callback receiving distance reading in mm */
	typedef void(*ObstacleCallback)(uint16_t);
	typedef void(*CollisionEnter)(uint16_t);
	typedef void(*CollisionExit)(uint16_t);

	void SetObstacleCallback(ObstacleCallback);
	void SetCollisionEnterCallback(CollisionEnter);
	void SetCollisionExitCallback(CollisionExit);

private:
	Adafruit_VL53L0X* m_tofSensor;
	EToFModes m_mode;
	uint8_t m_detectionFlags;	// None = 0, 1 = Obstacle, 2 = Collision
	FSensorBuffer<uint16_t> m_buffer;

	ObstacleCallback m_obstacleCb;
	CollisionEnter m_collisionEnterCb;
	CollisionExit m_collisionExitCb;
};
#endif // !EVF_SENSOR_TOF_H

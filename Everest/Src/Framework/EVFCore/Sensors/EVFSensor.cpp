/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#include "EVFSensor.h"
#include "Src/Framework/EVFUtils/EVFAssertions.h"
#include "Src/Framework/EVFUtils/EVFLogger.h"

using namespace EVF::Sensors;

Sensor::Sensor(uint8_t pinNumber, ESensorType sensorType)
	: m_sensorType(sensorType)
	, m_enabled(true)
	, m_pinNumber(pinNumber)
{
}

Sensor::~Sensor()
{

}

/* ITickableObject Interface */
void Sensor::Tick(float DeltaTime)
{
	//LOG("Tick in Base Sensor called!");
	check(false); // Should not run
}

bool Sensor::IsTickEnabled()
{
	return m_enabled;
}

void Sensor::SetSensorEnabled(bool enabled)
{
	m_enabled = enabled;
}

void Sensor::OnSensorValUpdated(const FSensorReading<>& reading)
{
	if (m_valUpdatedCb)
	{
		m_valUpdatedCb(m_sensorType,reading);
	}
}

bool Sensor::SetSensorValUpdatedCallback(SensorValUpdatedCallback callbackFunc)
{
	m_valUpdatedCb = callbackFunc;
}


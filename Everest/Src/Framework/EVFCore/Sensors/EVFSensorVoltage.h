#ifndef EVF_SENSOR_VOLTAGE_H
#define EVF_SENSOR_VOLTAGE_H

#include "EVFSensor.h"

namespace EVF
{
	namespace Sensors
	{
		class SensorVoltage;
	}
}

class EVF::Sensors::SensorVoltage : public EVF::Sensors::Sensor
{
public:
	SensorVoltage(uint8_t pinNumber);
	~SensorVoltage();

	virtual void Tick(float DeltaTime) override;

private:
};

#endif // !EVF_SENSOR_VOLTAGE_H


/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#include "EVFI2CBus.h"
#include "Wire.h"
#include "Arduino.h"

using namespace EVF::Sensors::I2CBus;

EI2CError WriteRegister(uint8_t deviceAddress, uint8_t registerAddress, uint8_t data)
{
	Wire.beginTransmission(deviceAddress); // open the device
	Wire.write(registerAddress); // write the register address
	Wire.write(data); // write the data
	Wire.endTransmission();

	delay(5);	// Wait for 5ms for the data to be written

	// Verify if our data is written to the register by simply reading it
	uint8_t buffer[1];
	EVF::Sensors::I2CBus::ReadRegister(deviceAddress, registerAddress, 1, buffer);	// TIL: Arrays are always passed by pointer, no & operator required

	if (buffer[1] != data)
		return EI2CError::WRITE_VALUE_MISMATCH;

	return EI2CError::NO_ERROR;
}

EI2CError ReadRegister(uint8_t deviceAddress, uint8_t registerAddress, uint8_t count, uint8_t* outData)
{
	Wire.beginTransmission(deviceAddress);
	Wire.write(registerAddress);
	uint8_t txnError = Wire.endTransmission(false);

	if (txnError)
		return EI2CError::READ_TXN_FAIL;

	uint8_t byteCount = Wire.requestFrom(registerAddress, count);

	// Unexpected byte count! Abort
	if (byteCount != count)
	{
		return EI2CError::READ_VALUE_MISMATCH;
	}
	return EI2CError::NO_ERROR;
}
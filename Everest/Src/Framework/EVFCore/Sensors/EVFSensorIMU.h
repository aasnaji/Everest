/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#ifndef EVF_SENSOR_IMU_H
#define EVF_SENSOR_IMU_H
#include "EVFSensor.h"
#include "Src/Framework/EVFSupport/Containers/EVFList.h"
#include "Src/Framework/EVFMath/EVMMath.h"

namespace EVF { namespace Sensors
{
	class SensorIMU;

	struct FIMUData
	{
		FVector Accel;
		FVector Gyro;
		float Temperature;

		/* Represents the structure that contains the IMU reading */
		FIMUData(const FVector& accel, const FVector& gyro, float temp)
			: Accel(accel)
			, Gyro(gyro)
			, Temperature(temp)
		{
		}

		FIMUData()
			: Accel(FVector())
			, Gyro (FVector())
			, Temperature(0)
		{
		}
	};
}}

class MPU9250;

/*	Sensor IMU
*
*	The software abstraction that provides an interface for
*	the IMU sensor connected to the robot. The IMU sensor data
*	can be queried every update cycle via the Tick method.
*	
*	The IMU sensor is capable of broadcasting specialized events
*	to signify a change in its status if necessary by following the
*	same delegation pattern implemented in EVF::Sensors::Sensor
*
*	Acceleration is reported in [m/s^2]
*	Angular Accel is reported in [deg/s]
*	Temperature is reported in [C]
*/
class EVF::Sensors::SensorIMU : public EVF::Sensors::Sensor
{
public:
	//SensorIMU(uint8_t pinNumber);
	SensorIMU();
	~SensorIMU();

	/* ITickableObject Interface */
	virtual void Tick(float DeltaTime) override final;	// Implement core sensor logic here

	/*	In case the IMU is not being read fast enough, request readings
	*	as per request. The new reading is normally added and managed 
	*	by the buffer.
	*/
	void RequestNewReading();

	/* Return read only values for the most recently processed IMU data*/
	FVector GetCurrAccel() const;
	FVector GetCurrGyro() const;
	FVector GetCurrMag() const;
	float GetCurrTemp() const;

	FVector GetLinearVelocity() const;
	FVector GetPosition() const;
	FVector GetRotation() const;

	/*	Sets the intensity of the averaging filter via a percentage alpha [0.0, 1.0]
	*	A percentage of 0 causes a blend that does not incorporate the filter effect.
	*	A percentage of 1 causes a blend where the filter is in full effect and slightly overdamped.
	*/
	void SetFilterSamples(uint8_t sampleCount);

	/*	Get Curr IMU Data
	*
	*	Returns the most recent, read-only, IMU measurements
	*	from the Accelerometer, Gyroscope, and Temperature sensor.
	*/
	const FIMUData& GetCurrIMUData() const;

	/*	Returns a read-only version of the internal buffer
	*	maintained by this sensor.
	*/
	inline const FSensorBuffer<FIMUData>& GetBuffer() const;

	/* Callback Functions & Bindings */
	
	/*	Signature of callback function to be invoked when the IMU
	*	detects a change in slope (ramp). Broadcasts the acceleration
	*	vector, current position and rotation
	*/
	typedef void(*SlopeDetectCallback)(const FSensorReading<FIMUData>&);
	void SetSlopeDetectCallback(SlopeDetectCallback cb);

private:
	/*	Processes the most recent readings in buffer and attempts
	*	to detect if a slope was encountered/
	*/
	void CheckForSlopes();

	/*	Implements a simple moving average filter to
	*	return a less noisy data output.
	*/
	void AverageFilter(FVector& accel, FVector& gyro);

	void PostProcessData(const FVector&, const FVector&, float);
private:
	MPU9250* m_imu;
	uint8_t m_filterSamples;
	FSensorBuffer<FIMUData> m_buffer;

	/*	Absolute values for velocity and rotation
	*	The IMU continously accumulates the integral
	*	while rejecting changes that do not exceed minimal thresholds.
	*/
	FVector m_position;
	FVector m_velocity;
	FVector m_rotation;

	/* Callbacks */
	SlopeDetectCallback m_slopeCb;
};

inline const EVF::Sensors::FSensorBuffer<EVF::Sensors::FIMUData>& 
EVF::Sensors::SensorIMU::GetBuffer() const
{
	return m_buffer;
}
#endif // !EVF_SENSOR_IMU_H

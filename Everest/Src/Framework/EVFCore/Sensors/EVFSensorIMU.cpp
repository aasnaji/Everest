/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#include "EVFSensorIMU.h"
#include "Src/Framework/EVFUtils/EVFLogger.h"

#include "Src/Libraries/IMU/src/MPU9250.h"

// MPU9250 I2C Address
#define IMU_I2C_ADDRESS 0x68
#define SLOPE_ACCEL_THRESHOLD  0.15	// The difference in the vertical component that indicates a slope detection
//#define DEBUG_IMU
using namespace EVF::Sensors;

EVF::Sensors::SensorIMU::SensorIMU()
	: Sensor(PIN_UNDEFINED, ESensorType::IMU) // Address 20 for I2C, this value is not required
	, m_imu (new MPU9250(Wire, IMU_I2C_ADDRESS))
	, m_position (FVector())
	, m_rotation (FVector())
	, m_filterSamples(4)
{
	// Initialization successful if returned 1
	if (m_imu->begin() != 1)
	{
		Serial.print("IMU Error Code: "); Serial.println();
		delay(1);
		check(false);
	}

	m_imu->setAccelRange(MPU9250::ACCEL_RANGE_8G);
	m_imu->setGyroRange(MPU9250::GYRO_RANGE_500DPS);
	m_imu->setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_20HZ);
	m_imu->setSrd(19);

	// Initialize the the filter cache to include the first measured reading
	m_imu->readSensor();

	FVector accel(	m_imu->getAccelZ_mss(),
					m_imu->getAccelY_mss(),
					m_imu->getAccelX_mss());

	FVector gyro(	m_imu->getGyroZ_rads(),
					m_imu->getGyroY_rads(),
					m_imu->getGyroX_rads());

	// Initialize Buffer
	for (int i = 0; i < 4; i++)
	{
		m_buffer.WriteReading(FSensorReading<FIMUData>(FIMUData(accel, gyro, 0)));
	}
}

SensorIMU::~SensorIMU()
{
	delete m_imu;
	m_imu = nullptr;
}

void SensorIMU::Tick(float DeltaTime)
{
	m_imu->readSensor();

	/*	The elements of the vector vary depending on mounting orientation, the current setup is as follows:
	*	
	*	X:	Up Direction (Gravity)
	*	Y:	Forward Direction (Drive)
	*	Z:	Right Direction (Turn)
	*/
	FVector accel(	m_imu->getAccelZ_mss(),
					m_imu->getAccelY_mss(),
					m_imu->getAccelX_mss());

	FVector gyro(	m_imu->getGyroZ_rads(),
					m_imu->getGyroY_rads(),
					m_imu->getGyroX_rads());

	// Use a moving average filter to smooth out noise
	AverageFilter(accel, gyro);

#ifdef DEBUG_IMU
	Serial.println(accel);
	delay(10);
#endif // DEBUG_IMU

	FSensorReading<FIMUData> reading(FIMUData(accel, gyro, m_imu->getTemperature_C()));

	// Process the integrals to acquire position and velocity
	PostProcessData(accel, gyro, DeltaTime);

	reading.TimeStamp = millis();
	m_buffer.WriteReading(reading);

	// Check if a sloped surface has been detected
	CheckForSlopes();
}


void SensorIMU::RequestNewReading()
{
	m_imu->readSensor();

	/*	The elements of the vector vary depending on mounting orientation, the current setup is as follows:
	*
	*	X:	Up Direction (Gravity)
	*	Y:	Forward Direction (Drive)
	*	Z:	Right Direction (Turn)
	*/
	FVector accel(	m_imu->getAccelZ_mss(),
					m_imu->getAccelY_mss(),
					m_imu->getAccelX_mss());

	FVector gyro(	m_imu->getGyroZ_rads(),
					m_imu->getGyroY_rads(),
					m_imu->getGyroX_rads());

	FSensorReading<FIMUData> reading(FIMUData(accel, gyro, m_imu->getTemperature_C()));

	// Process the integrals to acquire position and velocity
	float DeltaTime = millis() - m_buffer.GetReading().TimeStamp;
	PostProcessData(accel, gyro, DeltaTime);

	reading.TimeStamp = millis();
	m_buffer.WriteReading(reading);
}

/* IMU Accessors */
FVector SensorIMU::GetCurrAccel() const
{
	return m_buffer.GetReading().Reading.Accel;
}

FVector SensorIMU::GetCurrGyro() const
{
	return m_buffer.GetReading().Reading.Gyro;
}

FVector SensorIMU::GetCurrMag() const
{
	return FVector();
}

float SensorIMU::GetCurrTemp() const
{
	return m_buffer.GetReading().Reading.Temperature;
}

// Integrated IMU values

// Return the current linear velocity by integrating accelerometer data
FVector SensorIMU::GetLinearVelocity() const
{
	return FVector();
}

FVector SensorIMU::GetPosition() const
{
	return m_position;
}

FVector SensorIMU::GetRotation() const
{
	return m_rotation;
}


void SensorIMU::SetFilterSamples(uint8_t sampleCount)
{
	m_filterSamples = sampleCount;
}

const FIMUData& SensorIMU::GetCurrIMUData() const
{
	return m_buffer.GetReading().Reading;
}


void SensorIMU::SetSlopeDetectCallback(SlopeDetectCallback cb)
{
	m_slopeCb = cb;
}

void SensorIMU::CheckForSlopes()
{
	// No need to process if no one is using the information!
	if (!m_slopeCb)
		return;

	FSensorReading<FIMUData> imuData[4];
	m_buffer.GetReadings(imuData);

	// Take the difference between the most recent and oldest reading.
	// Recall this implementation defines the X-Axis as the vertical direction.
	float delta = imuData[4].Reading.Accel.X() - imuData[0].Reading.Accel.X();

	if (FMath::Abs(delta) > SLOPE_ACCEL_THRESHOLD)
	{
		m_slopeCb(imuData[4]);
	}
}

void SensorIMU::AverageFilter(FVector& accel, FVector& gyro)
{
	if (m_filterSamples == 0)
		return;

	FVector cumAccel = accel;
	FVector cumGyro = gyro;

	FSensorReading<FIMUData> data[4];

	m_buffer.GetReadings(data);

	for (int i = 0; i < m_filterSamples; i++)
	{
		if (i >= 4)
			break;

		cumAccel = cumAccel + data[i].Reading.Accel;
		cumGyro = cumGyro + data[i].Reading.Gyro;
	}

	accel = cumAccel / (m_filterSamples+1);
	gyro = cumGyro / (m_filterSamples+1);
}

void SensorIMU::PostProcessData(const FVector& _accel, const FVector& _gyro, float dt)
{
	FVector newVelocity = FVector::Integrate(_accel, m_buffer.GetReading().Reading.Accel, dt / 1000.0);
	m_position += FVector::Integrate(newVelocity, m_velocity, dt / 1000.0);
	m_velocity += newVelocity;
	//m_rotation += FVector::Integrate(_gyro, m_buffer.GetReading().Reading.Gyro, dt / 1000.0);
	m_rotation +=   _gyro * dt / 1000.0;
}

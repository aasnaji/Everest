/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#ifndef EVF_SENSOR_H
#define EVF_SENSOR_H

#include "Src/Framework/EVFCore/EVFITickableObject.h"
#include "Src/Framework/EVFSupport/Containers/EVFContainersFwd.h"
#include "Src/Framework/EVFUtils/EVFMemory.h"

#include <stdint.h>

// We constrain ourselves to a maximum of 4 listeners to a sensor's callback function
// Reduces complexity and code size to meet embedded system constraints
#define MAX_SENSOR_CB	(4)

// Reserve Pin Number 255 as an invalid or undefined pin. Simplifies our class heirarchy!! 
#define PIN_UNDEFINED	255

namespace EVF { namespace Sensors
{
	class Sensor;

	/*	The type of sensors the application can detect and dispatch events from
	*/
	enum class ESensorType : uint8_t
	{
		Undefined = 0x00,
		ToF = 0x01,
		Sonar = 0x02,
		IMU = 0x04,
		LimitSwitch = 0x08,
		CurrentSense = 0x10,
		IR = 0x20,
		VoltageSense = 0x40
	};

	// Templated version of a sensor reading
	template <typename T = uint16_t>
	struct FSensorReading
	{
		unsigned long TimeStamp;		// 32 bits
		T Reading;		

		FSensorReading<T>()
		{
		}

		FSensorReading<T>(T _reading)
			: Reading(_reading)
		{
		}

		void operator = (const FSensorReading<T> Other)
		{
			TimeStamp = Other.TimeStamp;
			Reading = Other.Reading;
		}
	};

	template <typename T = uint16_t>
	struct FSensorBuffer
	{
	private:
		FSensorReading<T> Readings[4]; // 256 bits worth of data
		uint8_t Indexer = 0;

	public:
		/* Emplaces a new reading into the buffer. The oldest reading gets overwritten */

		void WriteReading(const FSensorReading<T>& _readings)
		{	
			Readings[Indexer] = _readings;
			Indexer = Indexer == 3 ? 0 : Indexer + 1;	// The indexer wraps around the array of buffer, LIFO principle
		}

		/* Returns the most recent reading */
		FSensorReading<T> GetReading() const
		{
			return Readings[Indexer];
		}

		/* 
		   Outputs an array of size 4 that contains the buffer's data.
		   The output buffer is sorted such that the first elements is
		   the oldest reading.
		*/
		void GetReadings(FSensorReading<T> (&outReadings)[4]) const
		{
			int counter = 0;

			// The Indexer always points to our most reading. Since the
			// data is maintained in a circular list, the next element is
			// the oldest reading.
			for (int i = Indexer+1; counter < 4; i++)
			{
				if (i >= 4)
					i = 0;

				outReadings[counter] = Readings[i];
				counter++;
			}
		}

		FSensorBuffer<T>()
		{
		}
	};
}}



class EVF::Sensors::Sensor : public EVF::ITickableObject
{
public:

	/* ITickableObject Interface */
	virtual void Tick(float DeltaTime) override;	// Implement core sensor logic here
	virtual bool IsTickEnabled() override final;	// Base sensor class implementation is expressive enough, restrict overrides

	void SetSensorEnabled(bool enabled);
	inline bool GetSensorEnabled() const;
	
	/*	On Sensor Value Updated
	*	An Event that can be invoked from the sensor selectively.
	*	Invokes any delegates bound to this sensor's value updated event
	*
	*	@Param FSensorReading reading:	The measurement and timestamp to be broadcasted to listeners
	*/
	void OnSensorValUpdated(const FSensorReading<>& reading);

	/* Sensors use this signature to broadcast an event. The receiving method
	 * must be static for simplicity's sake */
	typedef void(*SensorValUpdatedCallback)(EVF::Sensors::ESensorType, FSensorReading<>);

	/*	Add Sensor Value Updated Delegate
	*	Binds a static delegate function to the sensor's value changed event
	*	Only 4 delegates can be bound, hence, always check for a return value of false!
	*
	*	@Param SensorCallbackFunc:	The function pointer that acts as the delegate/callback method that will be invoked with the event
	*/
	bool SetSensorValUpdatedCallback(SensorValUpdatedCallback callbackFunc);

	inline ESensorType GetSensorType() const;
	inline uint8_t		GetPinNumber() const;

protected:
	Sensor(uint8_t pinNumber, ESensorType sensorType);
	virtual ~Sensor();

protected:
	uint8_t m_pinNumber;

private:
	// The category or type of sensor - i.e IMU, Limit Switch, ToF etc.
	ESensorType m_sensorType;
	bool m_enabled;
	SensorValUpdatedCallback m_valUpdatedCb;
};


inline EVF::Sensors::ESensorType
EVF::Sensors::Sensor::GetSensorType() const
{
	return m_sensorType;
}

inline uint8_t	
EVF::Sensors::Sensor::GetPinNumber() const
{
	return m_pinNumber;
}

inline bool
EVF::Sensors::Sensor::GetSensorEnabled() const
{
	return m_enabled;
}
#endif // !EVF_SENSOR_H

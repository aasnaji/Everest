/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#ifndef EVF_I2C_BUS_H
#define EVF_I2C_BUS_H


#include <stdint.h>
/* This namespace provide common I2C utility for reading and writing to registers
*/
namespace EVF { namespace Sensors {namespace I2CBus
{
	enum EI2CError
	{
		NO_ERROR = 0x000,
		WRITE_TXN_FAIL = 0x001,
		WRITE_VALUE_MISMATCH = 0x002,
		READ_TXN_FAIL,
		READ_VALUE_MISMATCH,
	};

	/*	Write Register
	*
	*	Writes to individual registers within the IMU device.
	*	The I2C transmission should be started between the master
	*	and the I2C address of the sensor.
	*
	*	The subaddress represents the register address within the I2C slave device.
	*	The byte value, data, will be written to that register. If the value in the
	*	buffer does not reflect the provided data, WriteRegisterFail is returned.
	*
	*	@Param uint8 deviceAddress: The I2C address of the slave device that contains the register
	*	@Param uint8 subAddress: The address of the register to be written to within the slave device
	*	@Param uint8 data: The byte of data to be written to the register
	*	@Return EI2CError: WRITE_VALUE_MISMATCH if an error is encountered, NoError if successful
	*/
	EI2CError WriteRegister(uint8_t deviceAddress, uint8_t registerAddress, uint8_t data);

	/*	Read Register
	*
	*	Reads count number of bytes from a register within the IMU device
	*	via the provided register's subaddress. The data is output to a
	*	buffer, outData, which should at least allocated count number of bytes.
	*
	*	@Param uint8 deviceAddress: The I2C address of the slave device that contains the register
	*	@Param uint8 subAddress: The address of the register to be read from within the slave device
	*	@Param uint8 count: The number of bytes to request from the register
	*	@Param uint8* outData: The buffer/array that will contain the requested data. Must at least sized to fit count
	*	@Return EIMUError: READ_ if an error is encountered, NoError if successful
	*/
	EI2CError ReadRegister(uint8_t deviceAddress, uint8_t registerAddress, uint8_t count, uint8_t* outData);
}}}

#endif // !EVF_I2C_BUS_H
/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#ifndef EVF_ROBOT_CORE_H
#define EVF_ROBOT_CORE_H

#include "EVFRobotConstants.h"
#include "Src/Framework/EVFCore/EVFCoreFwd.h"
#include "EVFCore\EVFTickableRegistry.h"

#include "Servo.h"

namespace EVF
{
	class RobotCore;
	class ITickableObject;

	namespace Sensors
	{
		class Sensor;
		class SensorLimit;
		class SensorSonar;
		class SensorIMU;
		class SensorCurrent;
		class SensorToF;
	}

	namespace Servos
	{
		class ContinousServo;
		class PositionServo;
	}
}

namespace EV
{
	class RobotAI;
}


class EVF::RobotCore
{
public:
	RobotCore();
	~RobotCore();

	// Setup event system and initialize sensors here
	void RobotCoreInitialize();
	void RobotCoreHalt();			// Called when all equipments need to be powered off immediately!
	void Update();

	void HandleTickableRegistration(ITickableObject* tickable);

	/*	Component Accessors (Make these templated?)
	*/
	Sensors::Sensor* GetSensorOfType(Sensors::ESensorType type, uint8_t onPin = PIN_UNDEFINED) const;

	Servos::ContinousServo*			GetContinousServo(uint8_t pinId) const;
	Servos::PositionServo*			GetPositionServo(uint8_t pinId) const;

private:
	// Statically allocated - Must exist
	TickableRegistry m_tickableRegistry;

	/* The components equipped on this robot - sensors and servos */
	Sensors::Sensor*			m_sensors[ROBOT_SENSOR_COUNT];
	Servos::ContinousServo*		m_continousServos[ROBOT_CONT_SERVO_COUNT];
	Servos::PositionServo*		m_positionServos[ROBOT_POS_SERVO_COUNT];

	/* AI Behavioral Component */
	EV::RobotAI* m_robotAI;
};

extern EVF::RobotCore* ev_RobotCore;	// Globally Accessible

#endif // !EVF_ROBOT_CORE_H


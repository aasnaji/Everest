/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#ifndef EVF_ROBOT_CONSTANTS_H
#define EVF_ROBOT_CONSTANTS_H

#include "pins_arduino.h"

/*	The Robot constants declare I/O pins for a
*	pre-defined robot arrangement. The EVFRobot
*	is a ramp climbing robot employing the use of
*	2 servos and 7 sensors.
*
*	The Left and Right directions are relative to the
*	forward moving direction of the robot:
				-----------------
				|				|
	RIGHT		|				|       LEFT
				|				|
		H=======|		|		|=======H
		H	 ___|		|		|___    H
		H	|	|		|		|   |   H
		H	|___|		v		|___|   H
		H		-----------------       H
*/

/* Drive Continous Servos*/
#define L_DRIVESERVO_PIN				6
#define L_DRIVESERVO_SENSE_PIN			A2

#define R_DRIVESERVO_PIN				5
#define R_DRIVESERVO_SENSE_PIN			A3

/* Positional Servos */
#define L_ARMSERVO_PIN					4
#define L_ARMSERVO_SENSE_PIN			A4

#define R_ARMSERVO_PIN					3
#define R_ARMSERVO_SENSE_PIN			A5

/* Sonar */
#define SONAR_SERVO_PIN					2
#define SONAR_TRIGGER_PIN				8
#define SONAR_ECHO_PIN					7

/* IR (If Applicable) */
#define L_IR_PIN						A14
#define R_IR_PIN						A13

/* The Robot should not know or care about this.
   The IMU and ToF sensor objects should handle
   the I2C definitions internally. The robot is
   only responsible for reading/writing directly

#define IMU_I2C_ADDRESS					0x68
#define TOF_I2C_ADDRESS					0x78
*/

/* Limit Switches (If Applicable) */
#define L_LIMIT_SWITCH_0_PIN			9
#define L_LIMIT_SWITCH_1_PIN			10
#define R_LIMIT_SWITCH_0_PIN			11
#define R_LIMIT_SWITCH_1_PIN			12

/* Battery Monitor */
//#define BATTERY_VOLT_PIN				

/* I2C BUS SETTINGS */
#define I2C_CLK_SPEED					400000 //400 kHz High Speed I2C

/* ROBOT CORE COMPONENTS */
#define ROBOT_SENSOR_COUNT				7
#define ROBOT_POS_SERVO_COUNT			3
#define ROBOT_CONT_SERVO_COUNT			2

#endif // !EVF_ROBOT_CONSTANTS_H


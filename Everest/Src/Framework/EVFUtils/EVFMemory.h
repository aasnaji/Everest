#ifndef EVF_MEMORY_H
#define EVF_MEMORY_H



int freeRam();


/**
* Traits class which tests if a type is a pointer.
* I copied this from UE4 source - no idea how it works
* Template metaprograms are always mysterious...
*/
template <typename T>
struct TIsPointer
{
	enum { Value = false };
};

template <typename T> struct TIsPointer<               T*> { enum { Value = true }; };
template <typename T> struct TIsPointer<const          T*> { enum { Value = true }; };
template <typename T> struct TIsPointer<      volatile T*> { enum { Value = true }; };
template <typename T> struct TIsPointer<const volatile T*> { enum { Value = true }; };

template <typename T> struct TIsPointer<const          T> { enum { Value = TIsPointer<T>::Value }; };
template <typename T> struct TIsPointer<      volatile T> { enum { Value = TIsPointer<T>::Value }; };
template <typename T> struct TIsPointer<const volatile T> { enum { Value = TIsPointer<T>::Value }; };

#endif // EVF_MEMORY_H
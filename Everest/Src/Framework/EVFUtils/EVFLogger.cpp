#include "EVFLogger.h"
#include "Arduino.h"
#include "Print.h"

#ifdef EVF_DEBUG

void _LogMsg(const char* msg)
{
	Serial.println(msg);
	delay(5);
}

void _LogMsg(float msg)
{
	Serial.println(msg);
	delay(5);
}

void _LogWarning(const char* msg)
{
	Serial.print("[WARNING]: ");
	Serial.println(msg);
	delay(15);
}

void _LogError(const char* msg, const char * inFile, int inLine)
{
	delay(10);
	Serial.print("[ERROR] "); 

	Serial.println(msg);

	if( inFile)
	Serial.print("In File: "); Serial.println(inFile);

	Serial.print("In Line Number: "); Serial.println(inLine);
	Serial.println();

	delay(25);
}

#endif // EVF_DEBUG
#ifndef EVF_LOGGER_H
#define EVF_LOGGER_H

#include "EVFUtilConstants.h"

#if 0
	void _LogMsg(const char* msg);
	void _LogMsg(float msg);
	void _LogWarning(const char* msg);
	void _LogError(const char* msg, const char* inFile, int inLine);

	#define LOG(msg)					_LogMsg( (msg) );
	#define LOG_ERROR(msg)				_LogError((msg), (__FILE__), (__LINE__));
	#define LOG_WARNING(msg)			_LogWarning((msg));
#else
#define LOG(msg)				((void)0);			
#define LOG_ERROR(msg)			((void)0);			
#define LOG_WARNING(msg)		((void)0);
#endif // EVF_DEBUG

#endif // !EVF_LOGGER_H

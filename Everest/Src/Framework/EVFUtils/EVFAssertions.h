#ifndef EVF_ASSERTIONS_H
#define EVF_ASSERTIONS_H

#include "EVFUtilConstants.h"

#ifdef EVF_DEBUG

	#define check(expr)		if(!(expr)) LogAndAbort( nullptr ,(__FILE__), (__LINE__) );

	void LogAndAbort(const char* msg, const char* inFile, int inLine);

#else

#define check(expr)		((void)0);

#endif // EVF_DEBUG

#endif // !EVF_ASSERTIONS_H
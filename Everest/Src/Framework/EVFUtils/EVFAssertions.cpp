#include "EVFAssertions.h"
#include "Arduino.h"
#include "Print.h"
#include "Src/Framework/EVFRobotCore.h"

#ifdef EVF_DEBUG

void LogAndAbort(const char * msg, const char * inFile, int inLine)
{
	if (msg)
	{
		Serial.println("ASSERTION IN FILE: ");
		delay(10);
		Serial.println(inFile);
		Serial.println("\n");
		Serial.println("In Line No. ");
		delay(10);
		Serial.println(inLine);
		Serial.println("\n");
		delay(10);
		Serial.println(msg);
	}

	delay(10);

	// Emergency Stop the robot! Halts all motor/sensor operation so that we dont resume moving unexpectedly during an error
	if (ev_RobotCore)
	{
		ev_RobotCore->RobotCoreHalt();
	}

	abort();

}

#endif // _DEBUG


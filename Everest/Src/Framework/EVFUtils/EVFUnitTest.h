#ifndef EVF_UNIT_TEST_H
#define EVF_UNIT_TEST_H

namespace EVF
{
	class UnitTest;
}

class EVF::UnitTest
{
public:
	UnitTest();
	virtual ~UnitTest();

	void Start();

protected:
	virtual void OnExecuteTest() = 0;
	virtual void OnEndTest();
};

#endif // !EVF_UNIT_TEST_H

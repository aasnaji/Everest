#ifndef EVF_DELEGATE_H
#define EVF_DELEGATE_H
#include <stdint.h>

namespace EVF
{
	// A function pointer to a function with a single unknown parameter type
	typedef void(*AnonymousFunc)(void*);


	/* An anonymous, single parameter delegate object. The parameter is optional */
	struct FDelegate
	{
		// An identifier given to the delegate via the event management system upon registeration
		uint8_t Id;

		void Broadcast(void* param) const;
		void Bind(AnonymousFunc func);

		FDelegate();
	};
}
#endif // !EVF_DELEGATE_H

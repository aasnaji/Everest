#ifndef EVF_EVENT_MANAGER_H
#define EVF_EVENT_MANAGER_H

#include "EVFDelegate.h"
#include "Src/Framework/EVFSupport/Containers/EVFChain.h"
#include "Src/Framework/EVFSupport/Containers/EVFList.h"

namespace EVFEventManager
{
	/*	Register Delegate
	*	
	*	Adds a delegate to the managed list of application level delegate objects.
	*	There can only be a predefined number of delegates in a session; hence, an
	*	assertion is thrown if an attempt is made to register an out-of-bound delegate.
	*
	*	Register Callback
	*
	*	Bind a callback function with an Anonymous signature to the specified delegate object.
	*	Anonymous callbacks return void, and accept a single parameter of type void*.
	*	This introduces risks on the implementation side as users must be aware of the type 
	*	they are potentially receiving.
	*/
	void RegisterDelegate(EVF::FDelegate* InDelegate);
	void RegisterCallback(EVF::FDelegate* InDelegate, EVF::AnonymousFunc Callback);

	/*	On Broadcast Delegate
	*
	*	Invoked by a delegate when it attempts to broadcast its event
	*	Invokes all bound callback functions with the given argument Arg
	*/
	void OnBroadcastDelegate(uint8_t DelegateId, void* Arg);
	
}
#endif // !EVF_EVENT_MANAGER_H

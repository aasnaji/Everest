#include "EVFEventManager.h"
#include "assert.h"

using namespace EVFEventManager;
using namespace EVF;

#define MAX_DELEGATES 25


// Contains all delegates in the application
static TList<AnonymousFunc> CallbackList[MAX_DELEGATES];
uint8_t NextAvailableId = 0;

void EVFEventManager::RegisterDelegate(FDelegate* InDelegate)
{
	assert(NextAvailableId < MAX_DELEGATES);

	InDelegate->Id = NextAvailableId;
}

void EVFEventManager::RegisterCallback(FDelegate* InDelegate, AnonymousFunc Callback)
{
	assert(InDelegate->Id < MAX_DELEGATES);

	if (!CallbackList[InDelegate->Id].Find(Callback))
	{
		CallbackList[InDelegate->Id].Insert(Callback);
	}
}

void EVFEventManager::OnBroadcastDelegate(uint8_t DelegateId, void * Arg)
{
	assert(DelegateId < MAX_DELEGATES);
	
	auto It = CallbackList[DelegateId].Begin();

	for (; It != CallbackList[DelegateId].End(); It++)
	{
		// Invokes the callback, passing Arg as an argument
		It.Element->Data(Arg);
	}
}

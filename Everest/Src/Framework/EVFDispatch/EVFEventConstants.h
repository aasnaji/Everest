#ifndef EVF_EVENT_CONSTANTS_H
#define EVF_EVENT_CONSTANTS_H

/* 
   This file contains the basic definitions and enumerations that
   describe the events the robot can experience during operation
*/

namespace EVF
{
	enum EEventType : uint8_t
	{
		None = 0x00,
		SensorEvent = 0x01,
		BoardEvent = 0x02
	};


	/* Determines if the specified pin is occupied by an external sensor hardware
	*  or a built in signal. If the pin is unoccupied, returns EEventType::None
	*/
	EEventType GetEventType(uint8_t PinNumber)
}
#endif // !EVF_EVENT_CONSTANTS

#include "EVFDelegate.h"
#include "EVFEventManager.h"

using namespace EVF;

void EVF::FDelegate::Broadcast(void * param) const
{
	EVFEventManager::OnBroadcastDelegate(Id, param);
}

void FDelegate::Bind(AnonymousFunc cb)
{
	EVFEventManager::RegisterCallback(this, cb);
}

FDelegate::FDelegate()
{
	EVFEventManager::RegisterDelegate(this);
}
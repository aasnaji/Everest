/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#include "EVFTimerHandle.h"
#include "assert.h"

using namespace EVF;

TimerHandle::TimerHandle(ETimerPrescale prescale)
	: m_setTime(0)
{
}

TimerHandle::~TimerHandle()
{
}

TimerHandle::TimerHandle(const TimerHandle & OtherTimer)
{
}

TimerHandle& TimerHandle::operator=(const TimerHandle & OtherTimer)
{  
	// THIS SHOULD NEVER OCCUR!
	assert(false);
	TimerHandle handle;
	return handle;
}

void TimerHandle::SetCallbackFunction(TimerCallbackFunc func)
{
	m_CallbackFunction = func;
}

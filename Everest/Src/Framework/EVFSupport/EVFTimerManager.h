/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#ifndef EVF_TIMER_MANAGER_H
#define EVF_TIMER_MANAGER_H

#include "EVFTimerHandle.h"

#include "Src/Framework/EVFSupport/Containers/EVFList.h"

namespace EVF
{
	class TimerManager;

	// A Structure that contains a set timer and its target duration
	struct FTimerData
	{
		TimerHandle* timerHandle;
		float duration;

		bool operator ==(const FTimerData& Other)
		{
			return timerHandle == Other.timerHandle;
		}
	};
}

/*	Timer Manager
*
*	Manages the 3 Arduino hardware timers at (possibly) different clock rates.
*	The manager is responsible for implementing querying and storage
*	code so that we avoid duplication in every instance of the timer handle
*
*   The Everest framework reserves two timers for implementation. The Tick timer
*   is the equivalent of a globally running timer. It begins counting at the beginning
*   of every update cycle and is reset by the end of the update cycle. The timer is 
*	useful for providing elapsed duration of an update cycle, which can be used by
*	logic requiring the use of time steps in between measurements.
*
*	The second timer is reserved for general purpose duration timers. TimerHandles registered
*	with the secondary timer increment in 64 microseconds intervals. Such timers are wired with
*   an interrupt to signify an increment of 1 millisecond.
*/
class EVF::TimerManager
{

public:
	void SetTimer(TimerHandle& timerHandle, unsigned int duration_ms = 1000, bool periodic = false);
	void SetTimer(TimerHandle& timerHandle, TimerCallbackFunc callback, unsigned int duration_ms = 1000, bool periodic = false);

	/*	Get Elapsed Time
	*	
	*	Returns the milliseconds elapsed ever since timerHandle was set and bound.
	*	If the timer handle was never set by the Timer Manager, the result is always 0
	*
	*	@Param TimerHandle:		The timer object being queried
	*	@Return unsigned int:	The elapsed time for timerHandle. 0 if never set.
	*	@Note:					Although the global timer is stored in an unsigned long, we use an unsigned int
	*							because we do not expect a timer to run for more than 4 million seconds!
	*/
	unsigned int GetElapsedTime(const TimerHandle& timerHandle) const;

	/*	These methods are called when the Timer Manager is instantiated on
	*	the application level. Initialize registers of desired timers here.
	*
	*	Initialize Timer Control Register
	*	The clocking frequency is selected for each timer here
	*
	*	Initialize Interrupt
	*	Enable or disable interrupts for desired timers here
	*
	*	Reset Interrupt Flags
	*	Call the clear raised interrupt flags for ALL timers.
	*	Note that an ISR will always clear its respective interrupt flag
	*/
	void InitializeTimerControlRegister();
	void InitializeInterrupts();
	void ResetInterruptFlags();

private:
	TimerManager();
	~TimerManager();

	// Handles the addition of a new timer. Durations are maintained in a sorted list
	// where the last element is the next to receive an interrupt service. When the list
	// is empty, the timer's interrupt is disabled.
	void OnTimerSet(unsigned int addedDuration);

	void SetTimerCompareRegister(bool enabled, unsigned int compareVal_ms);

private:
};

#endif // !EVF_TIMER_MANAGER_H

/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#include "EVFTimerManager.h"

#include "Arduino.h"
#include "Print.h"

using namespace EVF;

unsigned int m_nextInterruptDuration = 0xFFFFFFFF;

// List of bound timers - these timers can be queried for their elapsed time by comparing
// their set time and the global timer using millis()
TList<FTimerData> m_secondaryTimers;

// If this is defined, enable the Input Capture Noise Canceler. Reduces noise in incoming
// signal, but requires 4 samples, and hence the signal is delayed by 4 oscillator cycles!
//#define TIM_NOISE_FILTER

// Binds the timer handle to the secondary timer that increments in milliseconds
void TimerManager::SetTimer(TimerHandle& timerHandle, unsigned int duration_ms, bool periodic)
{
	// Pro Tip: Taking the address of a reference is the same as taking the address of the object itself!
	FTimerData addedTimer {&timerHandle, duration_ms };
	FTimerData* foundTimer = m_secondaryTimers.Find(addedTimer);

	if (foundTimer)
	{
		// If that timer is already registered and running, reset it.
		foundTimer->timerHandle->m_setTime = millis();
		foundTimer->duration = 0;
	}
	else
	{
		// Add the timer to the managed list. It shall be updated in the ISR
		m_secondaryTimers.Insert(addedTimer);
	}
	
}

// Overload that sets the callback function for the timer handle
void TimerManager::SetTimer(TimerHandle& timerHandle, TimerCallbackFunc callback, unsigned int duration_ms, bool periodic)
{
	timerHandle.SetCallbackFunction(callback);

	SetTimer(timerHandle, duration_ms, periodic);
}

unsigned int EVF::TimerManager::GetElapsedTime(const TimerHandle & timerHandle) const
{
	return timerHandle.m_setTime == 0 ? 0 : millis() - timerHandle.m_setTime;
}

void TimerManager::InitializeTimerControlRegister()
{
	// Set the timer prescaler. TODO: VERIFY WORKINGS
	TCCR3B = 0;
	TCCR4B = 0;

	TCCR3B  |= (1 << CS30 | 0 << CS31 | 0 << CS32); // No Prescale = 62.5 nanosecond/Tick
	TCCR4B  |= (1 << CS40 | 0 << CS41 | 1 << CS42); // 1024 Prescale = 64 microsecond/Tick

	TCCR3B |= 1 << 6;  // Set Bit 6 - Captures input events on rising edge. Otherwise, falling edge
	TCCR4B |= 1 << 6;

#ifdef TIM_NOISE_FILTER
	TCCR3B |= 1 << 7; // Set Bit 7 - Input Capture Noise Canceler
	TCCR4B |= 1 << 7; 
#endif 

	// Continously compare our timer incremental value with a value of 16 Ticks in OCRA -- corresponding to 1 millisecond of time.
	// When reached, an interrupt is fired, and the bound timer values are incremented to reflect the change.
	OCR4A = 0x0010;
}

void TimerManager::InitializeInterrupts()
{
	// Enables the timer overflow interrupt (Bit 0)
	//TIMSK3 |= 1 << 0;

	TIMSK3 = 0;
	TIMSK4 |= 1 << 1;	// Enables Output A Compare interrupt
	TIMSK5 = 0;
}

// Used for initialization purposes only. We shouldn't need to reset the interrupt flags manually
void TimerManager::ResetInterruptFlags()
{
	// Clear all register flags
	TIFR3 = 1;
	TIFR4 = 1;
	TIFR5 = 1;
}

TimerManager::TimerManager()
{
	// Halt interrupts while we initialize ourselves
	noInterrupts();

	// Clear the timer to 0
	TCNT3 = 0;
	TCNT4 = 0;
	TCNT5 = 0;

	InitializeTimerControlRegister();
	ResetInterruptFlags();
	InitializeInterrupts();

	// Resume interrupts from here on
	interrupts();
}

TimerManager::~TimerManager()
{
}

void EVF::TimerManager::OnTimerSet(unsigned int addedDuration)
{
	if (addedDuration < m_nextInterruptDuration)
	{
		m_nextInterruptDuration = addedDuration;
		SetTimerCompareRegister(true, addedDuration);
	}
}

void EVF::TimerManager::SetTimerCompareRegister(bool enabled, unsigned int compareVal_ms)
{
	if (enabled)
	{
		TIMSK0 |= 1 << 1;	// Enables Output A Compare Interrupt
	}
	else
	{
		TIMSK0 &= ~(1 << 1); // Disables Output A Compare Interrupt
	}
}

#ifndef EVF_CHAIN_H
#define EVF_CHAIN_H

/*	A chain is a minimalist container that is a forwardly
*	iterated connection of nodes. Chains are ideal for scenarios
*	where heavy maintenance of the container is not required.
*/

namespace EVF
{
	template <typename DataType>
	struct TChainNode
	{
		TChainNode<DataType>* Next;
		DataType Data;

		TChainNode(const DataType& _data)
			: Data(_data)
			, Next(nullptr)
		{
		}

		TChainNode()
			: Next(nullptr)
		{
		}
	};
}

#endif // !EVF_CHAIN_H

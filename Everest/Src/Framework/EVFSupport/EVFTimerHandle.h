/**************************************************************************
* Copyright (C) 2018 - 2020, AnimalFarm, All rights reserved.
*
* This software is licensed under the MTE Public License v1.0
* which bares no legal significance whatsoever.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULARE PURPOSE AND NONINFRIGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************/

#ifndef EVF_TIMER_HANDLE_H
#define EVF_TIMER_HANDLE_H

#include "Arduino.h"

namespace EVF
{
	class TimerHandle;

	enum  ETimerPrescale
	{
		NONE		=	0,	// Every 62.5 nano-seconds
		CLK8,				// Every 0.5 micro-seconds
		CLK64,				// Every 4 micro-seconds
		CLK256,				// Every 16 micro-seconds
		CLK1024				// Every 64 micro-seconds
	};

	// Timer callback functions are restricted to void functions for cost and complexity
	typedef void(*TimerCallbackFunc)(float duration);
}

/*	TimerHandle
*
*	The Timer class offers a software abstraction for the built
*	in Arduino hardware timer. 
*
*	Timers are clocked by prescaling the system clock. The Timers
*	are triggered via a hardware interrupt (if required). A Timer
*	can also be used as a free running counter whose value can be
*	queried anytime via the Timer Manager.
*/
class EVF::TimerHandle
{
	friend class TimerManager;
public:
	TimerHandle(ETimerPrescale prescale = ETimerPrescale::NONE);
	~TimerHandle();

	inline TimerCallbackFunc GetCallbackFunction() const;
	inline unsigned int GetSetTime() const;
private:
	TimerHandle(const TimerHandle& OtherTimer);					// Not Copy-able
	TimerHandle& operator = (const TimerHandle& OtherTimer);	// Not Assignable

	void SetCallbackFunction(TimerCallbackFunc func);

public:

private:
	// Indicates the prescaled clock for this timer.
	// Timers can be 1, 1/8, 1/64, 1/256, 1/1024
	//ETimerPrescale m_timerPrescale;

	// The function to call if the timer finished ticking
	TimerCallbackFunc m_CallbackFunction;

	// The global time this Timer was started in milliseconds
	unsigned long m_setTime;
};


inline EVF::TimerCallbackFunc
EVF::TimerHandle::GetCallbackFunction() const
{
	return m_CallbackFunction;
}

inline unsigned int 
EVF::TimerHandle::GetSetTime() const
{
	return m_setTime;
}
#endif // !EVF_TIMER_HANDLE_H



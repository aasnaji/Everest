#include "EVFRobotCore.h"
#include "EVFCore\Sensors\EVFSensorLimit.h"
#include "EVFCore\Sensors\EVFSensorCurrent.h"
#include "EVFCore\Sensors\EVFSensorIMU.h"
#include "EVFCore\Sensors\EVFSensorSonar.h"
#include "EVFCore\Sensors\EVFSensorToF.h"
#include "EVFCore\Sensors\EVFSensorVoltage.h"

#include "Src/AI/EVSRobotAI.h"

#include "EVFUtils\EVFAssertions.h"
#include "EVFUtils\EVFMemory.h"

#include "Wire.h"

using namespace EVF;

RobotCore::RobotCore()
{
}

RobotCore::~RobotCore()
{

}

void RobotCore::RobotCoreInitialize()
{
	Serial.println("Initializing Robot Core!");
	delay(200);

	// Initialize I2C bus settings
	Wire.begin();
	Wire.setClock(I2C_CLK_SPEED);


	/* Subject to debate - move all construction to initialize function */
	// Construct all on board sensors - SUBJECT TO CHANGE
	m_sensors[0] = new Sensors::SensorIMU();
	m_sensors[1] = new Sensors::SensorSonar(SONAR_TRIGGER_PIN, SONAR_ECHO_PIN);
	m_sensors[2] = new Sensors::SensorToF();
	m_sensors[3] = new Sensors::SensorCurrent(L_DRIVESERVO_SENSE_PIN);
	m_sensors[4] = new Sensors::SensorCurrent(R_DRIVESERVO_SENSE_PIN);
	m_sensors[5] = new Sensors::SensorCurrent(L_ARMSERVO_SENSE_PIN);
	m_sensors[6] = new Sensors::SensorCurrent(R_ARMSERVO_SENSE_PIN);
	

	// Construct the Drive Servos
	m_continousServos[0] = new Servos::ContinousServo(L_DRIVESERVO_PIN, true);  // figure out direction convention experimentally!
	m_continousServos[1] = new Servos::ContinousServo(R_DRIVESERVO_PIN);

	// Construct the Arm Servos
	m_positionServos[0] = new Servos::PositionServo(L_ARMSERVO_PIN, true);
	m_positionServos[1] = new Servos::PositionServo(R_ARMSERVO_PIN);
	m_positionServos[2] = new Servos::PositionServo(SONAR_SERVO_PIN);

	Serial.println("ALL SENSORS INITIALIZED");
	delay(100);
	Serial.print("Remaining Memory: ");
	Serial.println(freeRam());

	check(m_continousServos[0] && m_continousServos[1]);
	check(m_positionServos[0] && m_positionServos[1] && m_positionServos[2]);

	m_robotAI = new EV::RobotAI(this);

	// After all is done, post initialize the tick system to finalize the state of the robot
	m_tickableRegistry.PostInitializeRegistry();
}

void RobotCore::RobotCoreHalt()
{
	for (Servos::ContinousServo* servo : m_continousServos)
	{
		if(servo)
			servo->Stop();
	}

	for (Servos::PositionServo* servo : m_positionServos)
	{
		if(servo)
			servo->Stop();
	}
		
}

void RobotCore::Update()
{
	m_tickableRegistry.ApplyTick();
}

void RobotCore::HandleTickableRegistration(ITickableObject * tickable)
{
	check(tickable);
	m_tickableRegistry.RegisterTickableObject(tickable);
}


Sensors::Sensor* RobotCore::GetSensorOfType(Sensors::ESensorType type, uint8_t onPin) const
{
	for (Sensors::Sensor* sensor : m_sensors)
	{
		if (!sensor)
			continue;

		if (sensor->GetSensorType() == type && sensor->GetPinNumber() == onPin)
			return sensor;
	}
	
	return nullptr;
}

/*	Potential code duplication here...store array of IBaseServo instead?
*/
Servos::ContinousServo* RobotCore::GetContinousServo(uint8_t pinId) const
{
	for (Servos::ContinousServo* servo : m_continousServos)
	{
		if (!servo)
			continue;

		if (servo->GetPinNumber() == pinId)
			return servo;
	}
	return nullptr;
}

Servos::PositionServo* RobotCore::GetPositionServo(uint8_t pinId) const
{
	for (Servos::PositionServo* servo : m_positionServos)
	{
		if (!servo)
			continue;

		if (servo->GetPinNumber() == pinId)
			return servo;
	}
	return nullptr;
}

// Pre-instantiation - For similar pattern, see arduino's Wire.h, or any extern variable semantics
RobotCore* ev_RobotCore = new RobotCore();
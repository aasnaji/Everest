#ifndef TEST_TICKABLE_H
#define TEST_TICKABLE_H

#include "Src/Framework/EVFDispatch/EVFDelegate.h"
#include "Src/Framework/EVFCore/Sensors/EVFSensor.h"
#include "HardwareSerial.h"


namespace EVFTest
{
	class TestTickable : public EVF::ITickableObject
	{
	public:
		TestTickable()
		{
			Serial.println("TestTickable Constructed!");
		}

		virtual void Tick(float DeltaTime) override
		{
			Serial.print("Tick TEST TICKABLE!");
			Serial.print("\n");
			delay(50);
		}
		virtual bool IsTickEnabled() override
		{
			return true;
		}
	};

	class TestSensor : public EVF::Sensors::Sensor
	{
	public:
		TestSensor()
			: EVF::Sensors::Sensor(PIN_UNDEFINED, EVF::Sensors::ESensorType::Undefined)
		{
			Serial.println("Test Sensor Constructed!");
			delay(100);
		}

		virtual void Tick(float DeltaTime) override
		{
			Serial.print("Tick TEST SENSOR ");
			Serial.print("\t");
			Serial.print(static_cast<int>(EVF::Sensors::Sensor::GetSensorType()));
			Serial.print("\n");
			delay(50);
		}
	};


	void StartTickableTest();

}

#endif // !TEST_TICKABLE_H

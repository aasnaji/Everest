#ifndef EVM_VECTORS_H
#define EVM_VECTORS_H
#include "EVMFunctions.h"
#include "Printable.h"
#include "Print.h"

#define EPSILON			(0.0001)
#define EPSILON_TINY	(0.0000001)

struct FVector : public Printable
{
public:
	inline FVector()
	{
	}

	inline FVector(float _x, float _y, float _z)
		: x(_x)
		, y(_y)
		, z(_z)
	{
	}

	inline float X() const { return x; }
	inline float Y() const { return y; }
	inline float Z() const { return z; }

	virtual size_t printTo(Print& p) const override
	{
		p.print("X: "); p.print(x); p.print("\t");
		p.print("Y: "); p.print(y); p.print("\t");
		p.print("Z: "); p.print(z); p.print("\t");
	}

private:
	float x;
	float y;
	float z;

public:

	inline bool operator == (const FVector& Other)			{ return x == Other.x && y == Other.y && z == Other.z; }
	inline FVector operator + (const FVector& Other) const	{ return FVector(x + Other.x, y + Other.y, z + Other.z); }
	inline FVector operator - (const FVector& Other) const	{ return FVector(x - Other.x, y - Other.y, z - Other.z); }
	inline FVector operator * (const FVector& Other) const	{ return FVector(x * Other.x, y * Other.y, z * Other.z); }	// Component-wise multiplication
	inline FVector operator / (const FVector& Other) const	{ return FVector(x / Other.x, y / Other.y, z / Other.z); }	// Component-wise division

	inline FVector operator * (float Scalar) const			{ return FVector(x * Scalar, y * Scalar, z * Scalar); }
	inline FVector operator / (float Scalar) const			{ return FVector(x / Scalar, y / Scalar, z / Scalar); }

	inline void operator += (const FVector& Other)		{ x += Other.x; y += Other.y; z += Other.z; }
	inline void operator -= (const FVector& Other)		{ x -= Other.x; y -= Other.y; z -= Other.z; }
	inline void operator *= (const FVector& Other)		{ x *= Other.x; y *= Other.y; z *= Other.z; }
	inline void operator /= (const FVector& Other)		{ x /= Other.x; y /= Other.y; z /= Other.z; }

	static inline float Dot(const FVector& A, const FVector& B)
	{
		return (  A.x * B.x
				+ A.y * B.y
				+ A.z * B.z);
	}

	static inline FVector Cross(const FVector& A, const FVector& B)
	{
		return FVector(	A.y * B.z - A.z * B.y,		// X-Component
						A.z * B.x - A.x * B.z,		// Y-Component
						A.x * B.y - A.y * B.x);		// Z-Component
	}

	static inline FVector Lerp(const FVector& A, const FVector& B, float alpha)
	{
		return FVector(	  FMath::Lerp(A.x, B.x, alpha)
						, FMath::Lerp(A.y, B.y, alpha)
						, FMath::Lerp(A.z, B.z, alpha));
	}
	
	/* Returns the angle between two vectors in radians */
	static inline float AngleBetween(const FVector& A, const FVector& B)
	{
		return FMath::ACosFast( (FVector::Dot(A, B) / (A.Length() * B.Length())) );
	}

	static inline FVector Integrate(const FVector& A, const FVector& B, float dt)
	{
		return FVector(   FMath::Cumtrapz(A.x, B.x, dt)
						, FMath::Cumtrapz(A.y, B.y, dt)
						, FMath::Cumtrapz(A.z, B.z, dt));
	}

	/* Returns the length of the vector using Sqrt(X^2 + Y^2 + Z^2) */
	inline float Length() const
	{
		return FMath::Sqrt(x*x + y*y + z*z);
	}

	inline float Length2D() const
	{
		return FMath::Sqrt(x*x + y*y);
	}

	/* Normalizes this vector to form a vector of magnitude of 1 */
	inline void Normalize()
	{
		*this * FMath::InvSqrt(x*x + y*y + z*z);
	}

	
	/*	Returns true if this vector is almost equal to the Other vector
	*	within a set threshold.
	*/
	inline bool IsNearlyEqualTo(const FVector& Other, float threshold = EPSILON)
	{
		return (*this - Other).Length() <= EPSILON;
	}

	/* Returns true if a vector is approximately equal to zero
	*  within a set threshold.
	*/
	inline bool IsNearlyZero(float threshold = EPSILON)
	{
		return Length() <= threshold;
	}

private:
};
#endif // !EVM_VECTORS_H

/ ****************************************************************************
	
	Application Structure

/ ****************************************************************************

Project Everest makes use of the MVC pattern in order to decouple data from algorithms.

	- The AI module contains the high level implementation that governs the robot's state machine,
	  drive mechanisms, and control systems.

	- The Framework module contains the core features that define the necessary hardware, background
	  systems, and utilities that acts as an API for high level programmers to implement the autonomous
	  logic of the robot.

	- The External Libraries module contain third party libraries and APIs to provide a quick interface
	  with sensors and other hardware components.


/ ****************************************************************************
	
	Programming Guidelines

/ **************************************************************************** 

-	The namespace convention must be followed for every class defined within a given module/folder
	
	* EVF : Stands for Everest Framework. Any class residing in the framework folder must be prefixed
			and scoped using the EVF namespace.
	
	* EVS : Stands for Everest/Everest Source. Any class residing the application level (i.e AI) must be
			prefixed and scoped using the EVS namespace.

	* EVAlg: Stands for Everest Algorithm. Any class residing in the EVSAlg that represents a form of algorithm
			must be prefixed and scoped using the EVAlg namespace.

-	Keep namespace prefixes to 2-3 letters per scope for brevity

-	Nested namespaces are used when a submodule is large enough to encompass a separate construct.
	For example: EVF::Sensors, EVF::Servos.

/ ****************************************************************************
	
	Arduino Guidelines, Programming Tips, and Troubleshooting

/ **************************************************************************** 

-	In order to successfully print messages to serial, both "Arduino.h" and "Print.h" must be defined.
	The Arduino header provides access to the Serial object, whereas the Print header provides the actual
	implementation necessary to use the serial's print functions.

-	The Serial will print garbage or cut-off data if misused. This occurs commonly in loops because the Serial cannot 
	print in time to keep up with subsequent instructions. Use delays in-between serial prints. Ensure the delays are
	only used in debug mode.

-	The "extern" keyword is only used as a DECLARATION! No allocation or definition occurs.
	To define an extern variable, carry out the initialization in a .cpp file as per required.
	As externs are global variables, they MUST be declared and defined in the global namespace; that is,
	externs cannot be used inside functions or classes.

	Example:
	In an .h file
					extern int globalNumber;

	In a .cpp file
					int globalNumber = 23;

	Notice the redeclaration of "int" is NECESSARY since the extern keyword does not allocate the actual size

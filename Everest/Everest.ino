/*
 Name:		Everest.ino
 Created:	9/26/2018 9:13:25 PM
 Author:	Naji
*/
#include "Src/Framework/EVFRobotCore.h"
#include "Src/AI/EVSRobotAI.h"

#include "Src/Framework/EVFUtils/EVFAssertions.h"

#include "Src/AI/EVSOverride/EVSOverrideController.h"

#include "Src/Libraries/AdafruitVL53L0/Adafruit_VL53L0X.h"
#include "Src/Libraries/IMU/src/MPU9250.h"

#include "Arduino.h"
#include "Wire.h"

#define DEBUG_TEST_TICKABLE
#ifdef DEBUG_TEST_TICKABLE
#include "Src/Framework/EVFTest/TestTickable.h"
#endif // Include test header

//#define CONTROL_OVERRIDE

/*	Robot Components
*
*	Initialize these components on the spot to ensure 
*	any dependent object have access to them reliably.
*	Alternatively, they can be set static.
*/
EVF::RobotCore* robotCore = nullptr;

#ifdef CONTROL_OVERRIDE
EVS::OverrideController* overrideController = nullptr;
#endif // CONTROL_OVERRIDE



// the setup fuction runs once when you press reset or power the board
void setup() 
{
	delay(50);
	Serial.begin(9600);	
	Serial.flush();
	delay(50);
	
	Serial.println("Initializing Everest!");
	robotCore = ev_RobotCore;
	robotCore->RobotCoreInitialize();

	delay(100);

#ifdef CONTROL_OVERRIDE
	overrideController = new EVS::OverrideController(robotCore);
#endif // CONTROL_OVERRIDE
}

// the loop function runs over and over again until power down or reset
void loop() 
{
	if (robotCore == NULL)
	{
		Serial.println("ROBOT CORE INVALID!");
		delay(15);
		check(false);
	}

	robotCore->Update();
	delay(20);
}
